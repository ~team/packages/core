# Maintainer (Arch): Tobias Powalowski <tpowa@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>

pkgname=kbd
pkgver=2.0.4
_debver=2.0.4
_debrel=4
pkgrel=5
pkgdesc="Keytable files and keyboard utilities"
arch=('i686' 'x86_64')
url="http://www.kbd-project.org"
license=('GPL-2')
# sh already have grep as package dependency
depends=('coreutils' 'sh' 'vlock')
makedepends=('check' 'quilt')
source=(https://www.kernel.org/pub/linux/utils/${pkgname}/${pkgname}-${pkgver}.tar.{gz,sign}
        https://deb.debian.org/debian/pool/main/k/kbd/kbd_$_debver-$_debrel.debian.tar.xz
        fix-euro2.patch)
sha512sums=('e37bc661c75a8363e9a5ba903310fa7f7ded4f381c2c77aa7edc0b1aca5a63224933fd113cddcf180e7fb85f55407e0d1f47be1cdf69dcf2787e83ac996bbf03'
            'SKIP'
            '17be15d11c9fd6f25711b91f6571b4f718373b755aaceb6ffb8b2cf227207e18fce278e439ea3b5c5f3c5de861eaf0f35db4b99ff34d36f9d91c3211e291e6e8'
            '17efb4a6591b218a1df8c831dee9c290fd53f693d340f46731c0452dcb6c86cee8fdf7569d022f22f33b46bf61bdddfa29da44366284652b3c7084650809c77b')
validpgpkeys=(
              '7F2A3D07298149A0793C9A4EA45ABA544CFFD434' #Alexey Gladkov
             )

prepare() {
  cd ${srcdir}/${pkgname}-${pkgver}
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
  # rename keymap files with the same names
  # this is needed because when only name of keymap is specified
  # loadkeys loads the first keymap it can find, which is bad (see FS#13837)
  # this should be removed when upstream adopts the change
  mv data/keymaps/i386/qwertz/cz{,-qwertz}.map
  mv data/keymaps/i386/olpc/es{,-olpc}.map
  mv data/keymaps/i386/olpc/pt{,-olpc}.map
  mv data/keymaps/i386/dvorak/no{,-dvorak}.map
  mv data/keymaps/i386/fgGIod/trf{,-fgGIod}.map
  mv data/keymaps/i386/colemak/{en-latin9,colemak}.map
  # fix euro2 #28213
  patch -Np1 -i ../fix-euro2.patch
}

build() {
  cd ${srcdir}/${pkgname}-${pkgver}
  ./configure --prefix=/usr --datadir=/usr/share/kbd --mandir=/usr/share/man --disable-vlock
  make KEYCODES_PROGS=yes RESIZECONS_PROGS=yes
}

package() {
  cd ${srcdir}/${pkgname}-${pkgver}
  make KEYCODES_PROGS=yes RESIZECONS_PROGS=yes DESTDIR=${pkgdir} install
  # we don’t want these in /usr/bin
  install -d -m755 ${pkgdir}/{,s}bin
  mv ${pkgdir}/usr/bin/{kbd_mode,setfont,fgconsole,openvt,chvt,dumpkeys,unicode_start,loadkeys} ${pkgdir}/bin
  mv ${pkgdir}/usr/bin/kbdrate ${pkgdir}/sbin
  # license
  install -Dm644 COPYING ${pkgdir}/usr/share/licenses/${pkgname}/COPYING
}
