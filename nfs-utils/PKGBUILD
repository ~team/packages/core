# Maintainer (Arch): AndyRTR <andyrtr@archlinux.org>
# Maintainer (Arch): Tobias Powalowski <tpowa@archlinux.org>
# Contributor (Arch): John Proctor <jproctor@prium.net>
# Contributor (Arch): dibblethewrecker <dibblethewrecker.at.jiwe.org>
# Contributor (Arch): abelstr <abel@pinklf.eu>
# Contributor (Arch): Marco Lima <cipparello gmail com>
# Contributor (Artix): artoo <artoo@cromnix.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgbase=nfs-utils
pkgname=('nfs-utils' 'nfsidmap')
pkgver=2.4.3
pkgrel=2
pkgdesc="Support programs for Network File Systems"
arch=('i686' 'x86_64')
url='http://nfs.sourceforge.net'
makedepends=('device-mapper' 'libevent' 'rpcbind' 'rpcsvc-proto' 'sqlite')
# http://git.linux-nfs.org/?p=steved/nfs-utils.git;a=summary
source=(https://www.kernel.org/pub/linux/utils/${pkgbase}/${pkgver}/${pkgbase}-${pkgver}.tar.{xz,sign}
        exports
        nfs.confd
        nfs.initd
        nfs.conf
        nfs.finish
        nfs.run
        nfs-client.confd
        nfs-client.initd
        rpc-blkmapd.run
        rpc-gssd.initd
        rpc-gssd.run
        rpc-idmapd.initd
        rpc-idmapd.run
        rpc-pipefs.initd
        rpc-svcgssd.initd
        rpc-svcgssd.run
        rpc-statd.initd
        rpc-statd.run)
optdepends=('sqlite: for nfsdcltrack usage'
            'python: for nfsiostat and mountstats usage')
# https://www.kernel.org/pub/linux/utils/nfs-utils/2.1.1/sha256sums.asc
sha512sums=('260359f06c1114915a96ee2a593137b02e24db03a414d8d52dadbccb272f7272cb2907e3877c824c80a5c9efeba09f5493c2c9144ef17e43e24c22b9e97cd605'
            'SKIP'
            '14dac48400d4bb790bbd30e118e24d0280713d3fb1f5af9884a6a4ca0480feb13dcc80e758f41fa58e3384977624af63ba3170353eac167ff3ee4c64c7ac50c2'
            '0fc267ca40da5bb03a4183cf35401a4cf8188c517d5a22fe0852c8d99e049855f52ed72b200ef61ea6135679f2b31d7712cdc15687b06afb2cefe19c0d0bd3ac'
            'bc22d48233dd7243249cf2dc849329127b64b665a1ce748fa9354dbd7737d220ca8d6629be46f71d3937b97fadce88c74100f9763d64fd4129aa390701ac28e5'
            'c1f35c7a5edf6869d98d58cb53550c1a877c9409ba47da6ff5d42d4f2e05f5bfbfb98a67ded5c4c8a4a0facebf23469a8e7643ae5651181cde069fd308d32b7e'
            '37cdfb13f85e9cd4d634b050e6e2745442358b1e3438068af4eea5496ef098ea46be754823c0c7ca8f5ae7735339db6410be3103c751b16d0f1c549534bbf553'
            '080e4dbfd8d95f3a42a04808024a13f01d46bdb0809eab51e6961726dc5ca5e542d836a53c82d719d00d344f7b860efb61905e664d754b61832ca31766825b4a'
            '0fbc06d7d5a02278ff39bc0a4cec387f37697bd2e7d46bfc75ee4049d4bcf5ad0a8846682b159cd836931544b56a8c0925dc423ed68eeac20aaea2acbd37e3a1'
            '13b6c148ad669910403f925b4815717e88c8a13e2b8147a93c63c408efdfc9f4df46daed7b1d75653daed114e41eb87b42f3142e3ce3b8f2439a6b124204c91c'
            '93bc91537e68bfe273550dab1d9a4ba34b8558e33170222efec0611aeaf7f5ba19ce4e199c16b34ec29917db7af4bb8b54f299ecfbdd27d3a3be408e1bac873d'
            '04f0f3b7eeee816f4a9aee258c7a0f9029e0176a5fb4016e3882255abdba3be208ff80b481a53789839338dc79b3b7b59b67d94c7a7798ffeb0b610cb462a94f'
            '35b680e349a0180797d3473c92e341077f1d042cdf4b61f25b40698325d3683b368753bde97ed9ac7f38f36291055fa6e28b15934e66959426cb2bc299134290'
            '146e0e203ebb92eeb7d273b19b06ff4e1d074754eba691c23c42ce1d46fc505c4b6c67eff33e496f5f0f5dbae76a7c6aaae51eb7e0d642e2687ff7fb06d03f50'
            'b1e996f8f616011f727c5390ed67dda106f6884ab6b96ddea1fbb27b0c23833b1298d76a6c3e14018c32db66a0bd0b4c5cd3891fc38b7624318675b75c87c11a'
            '3d3fa6e7ae01e27d27d88aba1e307c5293dd1cbc9d555737e03c4f1e17988518f4f1e7f7a38a7337f8e8961e8378cc4611769b04db7368460589a3b218ac6d6b'
            '06342fa459e842ead962c0e155aefbe9b94a352f04a97ac0c8489f3c5c6c910982232c3734ed223911a5b417f126a5da1c9354df127b7df4b19860b4b658255f'
            '476417f9bfc8e8207cb338196c83fcdfe6778b200eeb76a53b05b5800ea73f5bd82125b4af705b250f72f8a2a4a02c2e3ebc9abb603819d0037fdf36b842f9b6'
            'a0fe91b4d7d288cb4793335c79e715b3726f573674ef29525bdab20c2053d1ae19e53a76852ef038aee0ad24fb18827a4ff333a0a179263b1ba47c9a6e2d337c'
            'a8c3dd39f2edbe0679429571d4051ca9e832753182f92a2f264cae9c0ce9ebe0a113ac89b19d6c4a91d48a8eb5c391115de0c79671f73335f5f7e82abd09cdc9')
validpgpkeys=('E1B71E339E20A10A676F7CB69AFB1D681A125177') # Steve Dickson

prepare() {
  cd ${pkgbase}-${pkgver}
  autoreconf -vfi
}

build() {
  cd ${pkgbase}-${pkgver}
  ./configure --prefix=/usr \
    --sysconfdir=/etc \
    --enable-gss \
    --enable-svcgss \
    --without-tcp-wrappers \
    --with-statedir=/var/lib/nfs \
    --enable-ipv6 \
    --enable-libmount-mount \
    --enable-mountconfig \
    --with-start-statd=/usr/bin/start-statd
  make
}

check() {
  cd ${pkgbase}-${pkgver}
  make -k check
}

package_nfs-utils() {
  pkgdesc="Support programs for Network File Systems"
  license=('GPL-2')
  backup=(etc/{{exports,nfs.conf,nfsmount.conf},conf.d/nfs{,-client},sv/nfs/run})
  depends=('rpcbind' 'nfsidmap' 'quota-tools' 'gssproxy' 'libevent' 'device-mapper')
  optdepends=('sqlite: for nfsdcltrack usage'
              'python: for nfsiostat and mountstats usage')

  cd ${pkgbase}-${pkgver}
  make DESTDIR="$pkgdir" install

  install -D -m 644 utils/mount/nfsmount.conf "$pkgdir"/etc/nfsmount.conf
  install -D -m 644 nfs.conf "$pkgdir"/etc/nfs.conf

  # license
  install -D -m 644 COPYING "$pkgdir"/usr/share/licenses/nfs-utils/COPYING

  # docs
  install -d -m 755 "$pkgdir"/usr/share/doc/$pkgname
  install -m 644 {NEWS,README} "$pkgdir"/usr/share/doc/$pkgname/

  # empty exports file
  install -D -m 644 ../exports "$pkgdir"/etc/exports

  # config file for idmappers in newer kernels
  install -D -m 644 utils/nfsidmap/id_resolver.conf "$pkgdir"/etc/request-key.d/id_resolver.conf

  mkdir "$pkgdir"/etc/exports.d
  mkdir -m 555 "$pkgdir"/var/lib/nfs/rpc_pipefs
  mkdir "$pkgdir"/var/lib/nfs/v4recovery

  # OpenRC support
  for f in nfs nfs-client; do
    install -Dm644 "$srcdir"/$f.confd "$pkgdir"/etc/conf.d/$f
    install -Dm755 "$srcdir"/$f.initd "$pkgdir"/etc/init.d/$f
  done

  for f in rpc-gssd rpc-idmapd rpc-pipefs rpc-statd rpc-svcgssd; do
    install -Dm755 "$srcdir"/$f.initd "$pkgdir"/etc/init.d/$f
  done

  # runit support
  install -Dm755 "$srcdir"/nfs.finish "$pkgdir"/etc/sv/nfs/finish
  install -Dm644 "$srcdir"/nfs.conf "$pkgdir"/etc/sv/nfs/conf
  install -Dm755 "$srcdir"/nfs.run "$pkgdir"/etc/sv/nfs/run

  for f in rpc-blkmapd rpc-gssd rpc-idmapd rpc-statd rpc-svcgssd; do
    install -Dm755 "$srcdir"/$f.run "$pkgdir"/etc/sv/$f/run
  done

  # nfsidmap cleanup
  rm -vrf "$pkgdir"/usr/include #/nfsid*
  rm -vrf "$pkgdir"/usr/lib/libnfsidmap*
  rm -vrf "$pkgdir"/usr/lib/pkgconfig #/libnfsidmap.pc
  rm -v "$pkgdir"/usr/share/man/{man3/nfs4_uid_to_name*,man5/idmapd.conf*}
  rm -rf "$pkgdir"/usr/share/man/man3
}

package_nfsidmap() {
  pkgdesc="Library to help mapping IDs, mainly for NFSv4"
  license=('GPL-2')
  backup=(etc/idmapd.conf)
  depends=('libldap')

  cd "${pkgbase}"-${pkgver}
  make -C support  DESTDIR="$pkgdir" install

  # config file
  install -D -m 644 support/nfsidmap/idmapd.conf "$pkgdir"/etc/idmapd.conf

  # license
  install -Dm644 support/nfsidmap/COPYING $pkgdir/usr/share/licenses/nfsidmap/COPYING
}
