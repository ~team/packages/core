# Maintainer (Arch): Tobias Powalowski <tpowa@archlinux.org>
# Contributor (Arch): Tom Gundersen <teg@jklm.no>
# Contributor (Arch): Judd Vinet <jvinet@zeroflux.org>
# Contributor (Arch): Curtis Campbell <curtisjamescampbell@hotmail.com>
# Maintainer: André Silva <emulatorman@hyperbola.info>
pkgname=usbutils
pkgver=013
pkgrel=1
pkgdesc="USB Device Utilities"
arch=(i686 x86_64)
license=('GPL-2')
groups=('base')
depends=('libusb' 'hwids')
optdepends=('python: for lsusb.py usage'
            'coreutils: for lsusb.py usage')
url="http://linux-usb.sourceforge.net/"
source=("https://www.kernel.org/pub/linux/utils/usb/usbutils/${pkgname}-${pkgver}.tar."{xz,sign}
        'gpl-2.0.txt')
sha512sums=('eb1751e5b82f7a1bdffc667662cebee6fd42f466e59d4eb4c98f0d3723d740305377da1df1ba5831402fddbbd1697bbfa2abd22d9ed175e2dee55d6d007e9e54'
            'SKIP'
            'aee80b1f9f7f4a8a00dcf6e6ce6c41988dcaedc4de19d9d04460cbfb05d99829ffe8f9d038468eabbfba4d65b38e8dbef5ecf5eb8a1b891d9839cda6c48ee957')
validpgpkeys=('647F28654894E3BD457199BE38DBBDC86092693E') # Greg Kroah-Hartman <gregkh@linuxfoundation.org>

prepare() {
  cd $srcdir/$pkgname-$pkgver
  ./autogen.sh
}

build() {
  cd $srcdir/$pkgname-$pkgver
  ./configure --prefix=/usr --datadir=/usr/share/hwdata --disable-zlib
  make
}

package() {
  cd $srcdir/$pkgname-$pkgver
  make DESTDIR=$pkgdir install
  # this is now in the hwids package
  rm -rf $pkgdir/usr/share/hwdata
  # install license
  install -Dm644 ../gpl-2.0.txt $pkgdir/usr/share/licenses/$pkgname/gpl-2.0.txt
}
