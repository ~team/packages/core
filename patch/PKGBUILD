# Maintainer (Arch): Sébastien Luttringer <seblu@archlinux.org>
# Contributor (Arch): Allan McRae <allan@archlinux.org>
# Contributor (Arch): judd <jvinet@zeroflux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>

pkgname=patch
pkgver=2.7.6
_debver=2.7.6
_debrel=6
pkgrel=2
pkgdesc='A utility to apply patch files to original sources'
arch=('i686' 'x86_64')
url='https://www.gnu.org/software/patch/'
license=('GPL-3')
groups=('base-devel')
depends=('attr')
makedepends=('ed' 'quilt')
optdepends=('ed: for patch -e functionality')
validpgpkeys=('259B3792B3D6D319212CC4DCD5BF9FEB0313653A') # Andreas Gruenbacher
source=("https://ftp.gnu.org/gnu/$pkgname/$pkgname-$pkgver.tar.xz"{,.sig}
        "https://deb.debian.org/debian/pool/main/p/patch/patch_$_debver-$_debrel.debian.tar.xz"
        '19599883ffb6a450d2884f081f8ecf68edbed7ee.patch'
        '369dcccdfa6336e5a873d6d63705cfbe04c55727.patch'
        '9c986353e420ead6e706262bf204d6e03322c300.patch')
sha512sums=('fcca87bdb67a88685a8a25597f9e015f5e60197b9a269fa350ae35a7991ed8da553939b4bbc7f7d3cfd863c67142af403b04165633acbce4339056a905e87fbd'
            'SKIP'
            'b51c4361d71edde86e188d7511f66dc662afaaa5bc6c76c7bf1a99d0abef3d0de2db586d09b8d55b67cd8a0c3a8029570953e996fc639c1e8f926e24dc36bbb5'
            'eef6d7e70aaf5164460863fe718dbb69107ae9547d53201f27be7125677dd46e398df9d4611878b01b0dae08db4761743c61d275eacd85d061d5fd17758dfcd9'
            '351859c5ed1ad7a2021cc48fa4d0fdc2d01281e7d9aee76053a3d169308394d7d1a4122d43775a42c585bbaa697861cd0882139e23bfe60dbaede264052f11db'
            '5d2eaef629bae92e5b4e5e57d140c24a73e2811306d5f2854858f846646b034d2da315071f478bcf6f8d856a065b9bb073f76322e8e3a42616bc212281ce6945')

prepare() {
  cd $pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply and seems unimportant
    rm -v debian/patches/path_max || true

    quilt push -av
  else
    patch -p1 -i ../19599883ffb6a450d2884f081f8ecf68edbed7ee.patch
    patch -p1 -i ../369dcccdfa6336e5a873d6d63705cfbe04c55727.patch
  fi
  patch -p1 -i ../9c986353e420ead6e706262bf204d6e03322c300.patch
  autoreconf -fiv
}

build() {
  cd $pkgname-$pkgver
  ./configure --prefix=/usr
  make
}

check() {
  cd $pkgname-$pkgver
  make check
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="$pkgdir" install
  install -Dm644 COPYING $pkgdir/usr/share/licenses/$pkgname/COPYING
}

# vim:set ts=2 sw=2 et:
