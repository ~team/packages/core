# Maintainer: André Silva <emulatorman@hyperbola.info>
# Maintainer: Márcio Silva <coadde@hyperbola.info>
# Contributor: Luke R. <g4jc@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

# Based on binutils package

_ctarget=x86_64-unknown-hyperbolabsd
pkgname=$_ctarget-binutils220
pkgver=2.20.1
pkgrel=3
pkgdesc="A set of programs to assemble and manipulate binary and object files for HyperbolaBSD x86_64 target"
arch=('i686' 'x86_64')
url="https://www.gnu.org/software/binutils/"
license=('GPL-3' 'LGPL-3')
groups=('cross-devel')
depends=('zlib')
provides=("$_ctarget-binutils")
conflicts=("$_ctarget-binutils")
checkdepends=('dejagnu' 'bc')
options=('staticlibs' '!distcc' '!ccache')
source=(https://ftp.gnu.org/gnu/binutils/binutils-${pkgver}a.tar.bz2{,.sig}
        binutils-hyperbolabsd.patch)
validpgpkeys=('EAF1C276A747E9ED86210CBAC3126D3B4AE55E93') # Tristan Gingold <gingold@adacore.com>
sha512sums=('b05c93eb9ba8db344fbdee3c5b36ed5a7ad1366f948d41af43286715c7345a2477e0808d25f7bbf81b54ab06c4d46356d44318d56efcdbef42236d1a694411fc'
            'SKIP'
            '5c916c89b796c3a218719ab1846837717e423c816032f341e3f3d59ad17bf5922f0772ca701450f2cbaf68569ad8072627448bf778c1f6722be364d0fb5222cb')

prepare() {
  cd binutils-$pkgver

  # hack! - libiberty configure tests for header files using "$CPP $CPPFLAGS"
  sed -i "/ac_cpp=/s/\$CPPFLAGS/\$CPPFLAGS -O2/" libiberty/configure

  # HyperbolaBSD patch
  patch -p1 -i ${srcdir}/binutils-hyperbolabsd.patch

  mkdir ${srcdir}/binutils-build
}

build() {
  cd binutils-build

  # GPLv2 in binutils 2.17, GPLv3 in binutils 2.19
  # ld.gold is released in 2.19, but OpenBSD use 2.17 and 2.15 < 2.10
  ../binutils-$pkgver/configure \
    --prefix=/usr \
    --libdir=/usr/$_ctarget/lib \
    --with-lib-path=/lib:/usr/lib:/usr/local/lib \
    --with-bugurl=https://issues.hyperbola.info/ \
    --enable-threads \
    --enable-ld=default \
    --disable-gold \
    --enable-plugins \
    --enable-deterministic-archives \
    --with-pic \
    --disable-werror \
    --disable-gdb \
    --disable-nls \
    --target=$_ctarget

  # check the host environment and makes sure all the necessary tools are available
  make configure-host

  make CFLAGS="$CFLAGS -std=gnu89" CXXFLAGS="$CXXFLAGS -std=gnu++98"
}

check() {
  cd binutils-build

  # unset LDFLAGS as testsuite makes assumptions about which ones are active
  # ignore failures in gold testsuite...
  make -k LDFLAGS="" check || true
}

package() {
  cd binutils-build
  make prefix=${pkgdir}/usr libdir=${pkgdir}/usr/$_ctarget/lib install

  # Remove info documents that conflict with host version
  rm -rf ${pkgdir}/usr/share/info

  # install license files
  install -dm755 ${pkgdir}/usr/share/licenses/${pkgname}
  install -m644 ${srcdir}/binutils-$pkgver/COPYING3{,.LIB} ${pkgdir}/usr/share/licenses/${pkgname}
}
