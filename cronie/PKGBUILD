# Contributor (Arch): Kaiting Chen <kaiting.chen@kiwilight.com>
# Maintainer (Arch): Gaetan Bisson <bisson@archlinux.org>
# Contributor (Artix): artoo <artoo@cromnix.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Jesús E. <heckyel@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>

pkgname=cronie
pkgver=1.5.5
pkgrel=4
pkgdesc='Daemon that runs specified programs at scheduled times and related tools'
url='https://github.com/cronie-crond/cronie/'
license=('ISC')
arch=('i686' 'x86_64')
depends=('sh' 'run-parts')
optdepends=('smtp-server: send job output via email'
            'smtp-forwarder: forward job output to email server'
            'logger: message logging support')
source=("https://github.com/cronie-crond/cronie/releases/download/${pkgname}-${pkgver}/${pkgname}-${pkgver}.tar.gz"
        'deny'
        'cronie.confd'
        'cronie.initd'
        'cronie.conf'
        'cronie.run'
        'cronie_log.run')
sha512sums=('3cb53ee4db7722c0764a1d2e5dfe773cfc31c91e9d0798d579a58ec8c29a1f42238e18c27baae2eaff6df37250cd06dddbf77f0020aed68b9712fd7332efb912'
            'd05e3485cf39e94a4b1d185761a41a4e5476a0db228fd68b328381acde6a2ff3a7fe2c63938d7cd4ca03e13af035a40840ff15bc5aa0c37fee3bb99084bd835d'
            '1392b0bf396a2b06224f15fc0bcb088eade2e65ef463c7d63da5d1b0944c3e3648bc9139c1e698d0c55baabf62502a8e6fce1af2f39ffff30e0eba558022ccdd'
            '4fa75e91c131123659652eab45c9d2cda31b1ae26dcd6a0cbf40ff9b9cac077eb2eecf57ffeb1d8694748079858ee6451fd99d2c4f00d19b5a6f4b9b2b006272'
            '00b4f6774fd6f864344bc087ff0fafdd485293527db2a4bef0ca2ba37639383f56977c794687c345b705462c2ad442062205938ec9e5e79647aeba2f9f530537'
            '2b204299d74ebf5561d6cb5fd3dde5c0a597fa023863af26b152a0bebfbd6f0feda9612c688c91b84457f5eafdc9cef14063cd3a591d2d3a3cfed8fc8944ced3'
            '04ebc7e915c692cf185be820d41c226ec43cf1dc82aee04367cb88cdbdc3f0fb61cce2050d9478ada743781686ca8ca2b3e88bebe2ff1dbe146d9a7919fd8306')

backup=('etc/cron.deny'
        'etc/cron.d/0hourly'
        'etc/anacrontab'
        'etc/conf.d/cronie'
        'etc/sv/cronie/conf')

conflicts=('cron')
provides=('cron')

prepare() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  sed -i 's|/usr/bin/vi|/bin/vi|' configure.ac
  autoreconf -vfi
}

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  ./configure \
      --prefix=/usr \
      --sysconfdir=/etc \
      --localstatedir=/var \
      --enable-anacron \
      --with-inotify \
      --without-pam
  make
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  make DESTDIR="${pkgdir}" install

  chmod u+s "${pkgdir}"/usr/bin/crontab
  install -d "${pkgdir}"/var/spool/{ana,}cron
  install -d "${pkgdir}"/etc/cron.{d,hourly,daily,weekly,monthly}

  install -Dm644 ../deny "${pkgdir}"/etc/cron.deny

  install -Dm644 contrib/anacrontab "${pkgdir}"/etc/anacrontab
  install -Dm644 contrib/0hourly "${pkgdir}"/etc/cron.d/0hourly
  install -Dm755 contrib/0anacron "${pkgdir}"/etc/cron.hourly/0anacron

  install -Dm644 COPYING "${pkgdir}"/usr/share/licenses/cronie/COPYING

  install -Dm644 "${srcdir}"/cronie.confd "${pkgdir}"/etc/conf.d/cronie
  install -Dm755 "${srcdir}"/cronie.initd "${pkgdir}"/etc/init.d/cronie

  install -Dm644 "${srcdir}"/cronie.conf "${pkgdir}"/etc/sv/cronie/conf
  install -Dm755 "${srcdir}"/cronie.run "${pkgdir}"/etc/sv/cronie/run
  install -Dm755 "${srcdir}"/cronie_log.run "${pkgdir}"/etc/sv/cronie/log/run
}

# vim:set ts=2 sw=2 et:
