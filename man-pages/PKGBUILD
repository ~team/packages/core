# Maintainer (Arch): Andreas Radke <andyrtr@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=man-pages
pkgver=5.09
_debver=5.09
_debrel=2
pkgrel=2
pkgdesc="Manual pages about using a GNU/Linux system"
arch=('any')
license=('GPL-2')
url="http://man7.org/linux/man-pages/index.html"
groups=('base')
makedepends=('quilt')
source=(https://www.kernel.org/pub/linux/docs/man-pages/$pkgname-$pkgver.tar.{xz,sign}
        https://deb.debian.org/debian/pool/main/m/manpages/manpages_$_debver-$_debrel.debian.tar.xz
        copyright
        0006-dir_colors.5.patch)
sha512sums=('fa5684c316326afd2056876c174e4f2c8baa18d76bfc2f63009903f09f93b4c204fb0773e0991b5247e50596b2da7e232c5087121a0c938a072af8c77848686f'
            'SKIP'
            '7d8e9a0668efee5c03bc6232029b5f16813fbf2b8ad6c90dc0505ed9ec5d17a6015614a759154d37d09aacbb23d41df4850de2c9f1d70ed9cc1eb69e67f79118'
            'a197086a8c28502e84afe1048ca0f76bfaa1f8ab9d8019fed2e955634eeb2d6572ff72124c87ac926b8f0d117d0b2d7e67fa5fa0e1628af056690944899a28cd'
            'f73eb749c40715ba2543bc451263688d811ad819532608dda3a89e2bef78015da740ef83711655646db07b4ae22e687b8c28658118b02403ef14a6840a69fbcc')
validpgpkeys=('E522595B52EDA4E6BFCCCB5E856199113A35CE5E') # Michael Kerrisk (man-pages maintainer) <mtk.manpages@gmail.com>

prepare() {
  cd ${srcdir}/$pkgname-$pkgver

  # remove file reference about nonfree POSIX manual pages
  rm -v ../debian/POSIX-MANPAGES

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply and seems unimportant
    rm -v debian/patches/0006-dir_colors.5.patch || true
    rm -v debian/patches/0007-motd.5.patch || true

    quilt push -av
  fi
  # rebrand Debian patch
  patch -p1 -i ../0006-dir_colors.5.patch
}

package() {
  cd ${srcdir}/$pkgname-$pkgver

  make prefix=${pkgdir}/usr install

  # license
  install -Dm644 ../copyright $pkgdir/usr/share/licenses/$pkgname/copyright

  cd ${pkgdir}/usr/share/man
  # included in coreutils
  rm -f man1/{chgrp,chmod,chown,cp,dir,dd}.1
  rm -f man1/{df,dircolors,du,install,ln,ls}.1
  rm -f man1/{mkdir,mkfifo,mknod,mv,rm,rmdir}.1
  rm -f man1/{touch,vdir}.1
  # included in shadow
  rm -f man5/passwd.5
  rm -f man3/getspnam.3
  # included in diffutils
  rm -f man1/diff.1
  # included in tzdata
  rm -f man5/tzfile.5 man8/{tzselect,zdump,zic}.8
  # included in bpf
  rm -f man7/bpf-helpers.7
  # included in libxcrypt
  rm -f man3/crypt*.3
}
