# Maintainer (Arch):  Bartłomiej Piotrowski <bpiotrowski@archlinux.org>
# Contributor (Arch): Tobias Powalowski <tpowa@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=libusb
pkgver=1.0.26
pkgrel=1
pkgdesc="Library that provides generic access to USB devices"
arch=('i686' 'x86_64')
url='https://libusb.info/'
license=('LGPL-2.1')
depends=('libeudev')
makedepends=('eudev' 'quilt')
replaces=('libusb1' 'libusbx')
provides=("libusbx=$pkgver")
conflicts=('libusbx')
source=("https://github.com/libusb/libusb/releases/download/v$pkgver/${pkgname}-${pkgver}.tar.bz2")
sha512sums=('fcdb85c98f21639668693c2fd522814d440972d65883984c4ae53d0555bdbdb7e8c7a32199cd4b01113556a1eb5be7841b750cc73c9f6bda79bfe1af80914e71')

prepare() {
  cd $pkgname-$pkgver
  autoreconf -vfi
}

build() {
  cd $pkgname-$pkgver
  ./configure \
    --prefix=/usr
  make -j1
}

package () {
  make -C $pkgname-$pkgver DESTDIR="$pkgdir" install
  install -dm755 $pkgdir/lib
  mv $pkgdir/usr/lib/libusb-1.0.so.0* $pkgdir/lib
  ln -sf ../../lib/libusb-1.0.so.0 $pkgdir/usr/lib/libusb-1.0.so
  install -Dm644 $pkgname-$pkgver/COPYING $pkgdir/usr/share/licenses/$pkgname/COPYING
}
