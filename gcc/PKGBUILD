# Maintainer (Arch): Allan McRae <allan@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Luke R. <g4jc@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

# toolchain build order: linux-libre-lts-api-headers->glibc->binutils->gcc->binutils->glibc
# NOTE: libtool requires rebuilt with each new gcc version

pkgname=('gcc' 'gcc-libs' 'gcc-fortran' 'gcc-objc' 'gcc-ada' 'gcc-go')
pkgver=8.4.0
_debver=8.4.0
_debrel=4
_pkgver=8
_islver=0.21
pkgrel=6
pkgdesc="The GNU Compiler Collection"
arch=('i686' 'x86_64')
license=('GPL-3' 'LGPL-3' 'custom:GCC-Exception-3.1' 'FDL-1.3')
url='https://gcc.gnu.org'
depends=('binutils>=2.34' 'libmpc')
makedepends=('gcc-ada' 'doxygen' 'quilt') # quilt already has libxcrypt as dependency
checkdepends=('dejagnu' 'net-tools')
options=('!emptydirs')
source=(https://ftp.gnu.org/gnu/gcc/gcc-$pkgver/gcc-$pkgver.tar.xz
        https://repo.hyperbola.info:50000/sources/gcc/gcc-8_$_debver-$_debrel.diff.gz{,.sig}
        https://repo.hyperbola.info:50000/sources/isl/isl-${_islver}.tar.bz2{,.sig}
        COPYING.DOC)
sha512sums=('6de904f552a02de33b11ef52312bb664396efd7e1ce3bbe37bfad5ef617f133095b3767b4804bc7fe78df335cb53bc83f1ac055baed40979ce4c2c3e46b70280'
            '76c3246c83a97bccafbd98e9bb60586beac1497746962c94e28559534973aff4065c53fc1595625caf5541febc66b564befeed29aa6ba4e647c1ca65db9b1ef0'
            'SKIP'
            '48f3b8d90550e8ab28837b5757f87bf99cddec67769877e04942abef69bbe526ef4c863951d55dd89a6027dc09df48988c8df6029782f990aa4d5b67e65f6d53'
            'SKIP'
            'bea1788b2bdc84f470e459114b871cf4ee991718964a63e18adde65116806d7676484cb30857cf74dece5eef5f96a015ee4a21900e019623e5d3484868b28b7f')
validpgpkeys=('C92BAA713B8D53D3CAE63FC9E6974752F9704456') # André Silva

_libdir="usr/lib/gcc/$CHOST/$pkgver"

prepare() {
  cd ${srcdir}/gcc-$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    patch -p1 -i ../gcc-8_$_debver-$_debrel.diff

    cd debian/patches
    ls -1 > series
    sed -ri 's/(.)$$/\1 -p2/' series
    sed -i '\/series \-p2/d' series

    cd ${srcdir}/gcc-$pkgver

    # Doesn't apply
    rm -v debian/patches/ada-arm.diff || true
    rm -v debian/patches/ada-armel-libatomic.diff || true
    rm -v debian/patches/ada-changes-in-autogen-output.diff || true
    rm -v debian/patches/ada-drop-termio-h.diff || true
    rm -v debian/patches/ada-gcc-name.diff || true
    rm -v debian/patches/ada-gnattools-cross.diff || true
    rm -v debian/patches/ada-kfreebsd.diff || true
    rm -v debian/patches/ada-libgnatvsn.diff || true
    rm -v debian/patches/ada-lib-info-file-prefix-map.diff || true
    rm -v debian/patches/ada-lib-info-source-date-epoch.diff || true
    rm -v debian/patches/ada-link-lib.diff || true
    rm -v debian/patches/ada-nobiarch-check.diff || true
    rm -v debian/patches/ada-sjlj.diff || true
    rm -v debian/patches/add-gnu-to-libgo-headers.diff || true
    rm -v debian/patches/add-gnu-to-libgo-test-headers.diff || true
    rm -v debian/patches/alpha-ieee.diff || true
    rm -v debian/patches/alpha-ieee-doc.diff || true
    rm -v debian/patches/alpha-no-ev4-directive.diff || true
    rm -v debian/patches/arm-multilib-defaults.diff || true
    rm -v debian/patches/arm-multilib-soft-cross.diff || true
    rm -v debian/patches/arm-multilib-soft.diff || true
    rm -v debian/patches/arm-multilib-soft-float.diff || true
    rm -v debian/patches/arm-multilib-softfp-cross.diff || true
    rm -v debian/patches/arm-multilib-softfp.diff || true
    rm -v debian/patches/bind_now_when_pie.diff || true
    rm -v debian/patches/bootstrap-no-unneeded-libs.diff || true
    rm -v debian/patches/canonical-cpppath.diff || true
    rm -v debian/patches/config-ml.diff || true
    rm -v debian/patches/cross-biarch.diff || true
    rm -v debian/patches/cross-fixes.diff || true
    rm -v debian/patches/cross-install-location.diff || true
    rm -v debian/patches/cross-no-locale-include.diff || true
    rm -v debian/patches/cuda-float128.diff || true
    rm -v debian/patches/disable-gdc-tests.diff || true
    rm -v debian/patches/gcc-alpha-bs-ignore.diff || true
    rm -v debian/patches/gcc-as-needed.diff || true
    rm -v debian/patches/gcc-as-needed-gold.diff || true
    rm -v debian/patches/gcc-auto-build.diff || true
    rm -v debian/patches/gcc-default-fortify-source.diff || true
    rm -v debian/patches/gcc-default-relro.diff || true
    rm -v debian/patches/gcc-distro-specs.diff || true
    rm -v debian/patches/gcc-distro-specs-doc.diff || true
    rm -v debian/patches/gcc-d-lang.diff || true
    rm -v debian/patches/gcc-driver-extra-langs.diff || true
    rm -v debian/patches/gcc-driver-extra-langs-linaro.diff || true
    rm -v debian/patches/gcc-force-cross-layout.diff || true
    rm -v debian/patches/gcc-fuse-ld-lld.diff || true
    rm -v debian/patches/gcc-fuse-ld-lld-doc.diff || true
    rm -v debian/patches/gcc-gfdl-build.diff || true
    rm -v debian/patches/gcc-hash-style-both.diff || true
    rm -v debian/patches/gcc-hash-style-gnu.diff || true
    rm -v debian/patches/gcc-ice-apport.diff || true
    rm -v debian/patches/gcc-ice-dump.diff || true
    rm -v debian/patches/gcc-linaro.diff || true
    rm -v debian/patches/gcc-linaro-doc.diff || true
    rm -v debian/patches/gcc-linaro-no-macros.diff || true
    rm -v debian/patches/gcc-linaro-revert-r270683.diff || true
    rm -v debian/patches/gcc-multiarch.diff || true
    rm -v debian/patches/gcc-multilib-multiarch.diff || true
    rm -v debian/patches/gcc-search-prefixed-as-ld.diff || true
    rm -v debian/patches/gcc-target-include-asm.diff || true
    rm -v debian/patches/gcc-textdomain.diff || true
    rm -v debian/patches/gdc-8.diff || true
    rm -v debian/patches/gdc-8-doc.diff || true
    rm -v debian/patches/gdc-cross-biarch.diff || true
    rm -v debian/patches/gdc-cross-build.diff || true
    rm -v debian/patches/gdc-cross-install-location.diff || true
    rm -v debian/patches/gdc-driver-nophobos.diff || true
    rm -v debian/patches/gdc-frontend-posix.diff || true
    rm -v debian/patches/gdc-libphobos-build.diff || true
    rm -v debian/patches/gdc-multiarch.diff || true
    rm -v debian/patches/gdc-profiledbuild.diff || true
    rm -v debian/patches/gdc-shared-by-default.diff || true
    rm -v debian/patches/gdc-sparc-fix.diff || true
    rm -v debian/patches/gdc-targetdm.diff || true
    rm -v debian/patches/gdc-targetdm-doc.diff || true
    rm -v debian/patches/gdc-texinfo.diff || true
    rm -v debian/patches/gdc-updates.diff || true
    rm -v debian/patches/git-doc-updates.diff || true
    rm -v debian/patches/git-updates.diff || true
    rm -v debian/patches/g++-multiarch-incdir.diff || true
    rm -v debian/patches/go-testsuite.diff || true
    rm -v debian/patches/hurd-changes.diff || true
    rm -v debian/patches/ia64-disable-selective-scheduling.diff || true
    rm -v debian/patches/ignore-pie-specs-when-not-enabled.diff || true
    rm -v debian/patches/kfreebsd-decimal-float.diff || true
    rm -v debian/patches/kfreebsd-unwind.diff || true
    rm -v debian/patches/libasan-sparc.diff || true
    rm -v debian/patches/libffi-mips.diff || true
    rm -v debian/patches/libffi-mipsen-r6.diff || true
    rm -v debian/patches/libffi-pax.diff || true
    rm -v debian/patches/libffi-race-condition.diff || true
    rm -v debian/patches/libffi-riscv64-go.diff || true
    rm -v debian/patches/libffi-riscv.diff || true
    rm -v debian/patches/libffi-ro-eh_frame_sect.diff || true
    rm -v debian/patches/libgo-cleanfiles.diff || true
    rm -v debian/patches/libgomp-kfreebsd-testsuite.diff || true
    rm -v debian/patches/libgomp-omp_h-multilib.diff || true
    rm -v debian/patches/libgo-revert-timeout-exp.diff || true
    rm -v debian/patches/libgo-setcontext-config.diff || true
    rm -v debian/patches/libgo-testsuite.diff || true
    rm -v debian/patches/libitm-no-fortify-source.diff || true
    rm -v debian/patches/libjit-ldflags.diff || true
    rm -v debian/patches/libphobos-zlib.diff || true
    rm -v debian/patches/libstdc++-doclink.diff || true
    rm -v debian/patches/libstdc++-futex.diff || true
    rm -v debian/patches/libstdc++-man-3cxx.diff || true
    rm -v debian/patches/libstdc++-no-testsuite.diff || true
    rm -v debian/patches/libstdc++-nothumb-check.diff || true
    rm -v debian/patches/libstdc++-pic.diff || true
    rm -v debian/patches/libstdc++-test-installed.diff || true
    rm -v debian/patches/linaro-issue2575.diff || true
    rm -v debian/patches/note-gnu-stack.diff || true
    rm -v debian/patches/powerpcspe_nofprs.diff || true
    rm -v debian/patches/powerpcspe_remove_many.diff || true
    rm -v debian/patches/pr39491.diff || true
    rm -v debian/patches/pr67899.diff || true
    rm -v debian/patches/pr81829.diff || true
    rm -v debian/patches/pr87338.diff || true
    rm -v debian/patches/pr90714.diff || true
    rm -v debian/patches/rename-info-files.diff || true
    rm -v debian/patches/skip-bootstrap-multilib.diff || true
    rm -v debian/patches/sparc64-biarch-long-double-128.diff || true
    rm -v debian/patches/src_gcc_config_i386_gnu.h.diff || true
    rm -v debian/patches/src_libgo_build.diff || true
    rm -v debian/patches/src_libgo_go_crypto.diff || true
    rm -v debian/patches/src_libgo_go_go_build_syslist.go.diff || true
    rm -v debian/patches/src_libgo_go_net.diff || true
    rm -v debian/patches/src_libgo_go_os.diff || true
    rm -v debian/patches/src_libgo_go_runtime.diff || true
    rm -v debian/patches/src_libgo_go_syscall.diff || true
    rm -v debian/patches/src_libgo_go_syscall_syscall_errno.go.diff
    rm -v debian/patches/src_libgo_go_syscall_syscall_gnu_test.go.diff || true
    rm -v debian/patches/src_libgo_runtime.diff || true
    rm -v debian/patches/src_libgo_testsuite_gotest.diff || true
    rm -v debian/patches/sys-auxv-header.diff || true
    rm -v debian/patches/testsuite-glibc-warnings.diff || true
    rm -v debian/patches/testsuite-hardening-format.diff || true
    rm -v debian/patches/testsuite-hardening-printf-types.diff || true
    rm -v debian/patches/testsuite-hardening-updates.diff || true
    rm -v debian/patches/t-libunwind-elf-Wl-z-defs.diff || true
    rm -v debian/patches/verbose-lto-linker.diff

    quilt push -av
  fi

  # link isl for in-tree build
  ln -s ../isl-${_islver} isl

  # Do not run fixincludes
  sed -i 's@\./fixinc\.sh@-c true@' gcc/Makefile.in

  # Hyperbola installs x86_64 libraries /lib
  [[ $CARCH == "x86_64" ]] && sed -i '/m64=/s/lib64/lib/' gcc/config/i386/t-linux64

  # hack! - some configure tests for header files using "$CPP $CPPFLAGS"
  sed -i "/ac_cpp=/s/\$CPPFLAGS/\$CPPFLAGS -O2/" {libiberty,gcc}/configure

  mkdir ${srcdir}/gcc-build
}

build() {
  cd ${srcdir}/gcc-build

  # using -pipe causes spurious test-suite failures
  # http://gcc.gnu.org/bugzilla/show_bug.cgi?id=48565
  CFLAGS=${CFLAGS/-pipe/}
  CXXFLAGS=${CXXFLAGS/-pipe/}

  ${srcdir}/gcc-$pkgver/configure --prefix=/usr \
      --libdir=/usr/lib \
      --libexecdir=/usr/libexec \
      --mandir=/usr/share/man \
      --infodir=/usr/share/info \
      --with-bugurl=https://issues.hyperbola.info/ \
      --enable-languages=c,c++,ada,fortran,go,lto,objc,obj-c++ \
      --enable-shared \
      --enable-threads=posix \
      --enable-libmpx \
      --with-system-zlib \
      --with-isl \
      --enable-__cxa_atexit \
      --disable-libunwind-exceptions \
      --enable-clocale=gnu \
      --disable-libstdcxx-pch \
      --disable-libssp \
      --enable-gnu-unique-object \
      --enable-linker-build-id \
      --enable-lto \
      --enable-plugin \
      --enable-install-libiberty \
      --with-linker-hash-style=gnu \
      --enable-gnu-indirect-function \
      --disable-multilib \
      --disable-werror \
      --enable-checking=release

  make
  
  # make documentation
  make -C $CHOST/libstdc++-v3/doc doc-man-doxygen
}

package_gcc-libs()
{
  pkgdesc="Runtime libraries shipped by GCC"
  groups=('base')
  depends=('zlib')
  options=('!emptydirs' '!strip')

  cd ${srcdir}/gcc-build
  
  make -C $CHOST/libgcc DESTDIR=${pkgdir} install-shared
  rm ${pkgdir}/${_libdir}/libgcc_eh.a
  
  for lib in libatomic \
             libgfortran \
             libgo \
             libgomp \
             libitm \
             libquadmath \
             libsanitizer/{a,l,ub}san \
             libstdc++-v3/src \
             libvtv; do
    make -C $CHOST/$lib DESTDIR=${pkgdir} install-toolexeclibLTLIBRARIES
  done

  [[ $CARCH == "x86_64" ]] && \
    make -C $CHOST/libsanitizer/tsan DESTDIR=${pkgdir} install-toolexeclibLTLIBRARIES

  make -C $CHOST/libobjc DESTDIR=${pkgdir} install-libs

  make -C $CHOST/libstdc++-v3/po DESTDIR=${pkgdir} install

  make -C $CHOST/libmpx DESTDIR=${pkgdir} install
  rm ${pkgdir}/usr/lib/libmpx.spec

  for lib in libgomp \
             libitm \
             libquadmath; do
    make -C $CHOST/$lib DESTDIR=${pkgdir} install-info
  done

  # Add licenses
  install -m755 -d ${pkgdir}/usr/share/licenses/gcc-libs
  install -m644 ${srcdir}/gcc-$pkgver/COPYING{3{,.LIB},.RUNTIME} ${srcdir}/COPYING.DOC \
    ${pkgdir}/usr/share/licenses/gcc-libs
}

package_gcc()
{
  pkgdesc="The GNU Compiler Collection - C and C++ frontends"
  depends=("gcc-libs=$pkgver-$pkgrel" 'binutils>=2.34' 'libmpc')
  groups=('base-devel')
  options=('staticlibs')

  cd ${srcdir}/gcc-build

  make -C gcc DESTDIR=${pkgdir} install-driver install-cpp install-gcc-ar \
    c++.install-common install-headers install-plugin install-lto-wrapper

  install -m755 -t $pkgdir/usr/bin/ gcc/gcov{,-tool}
  install -m755 -t $pkgdir/${_libdir}/ gcc/{cc1,cc1plus,collect2,lto1}

  make -C $CHOST/libgcc DESTDIR=${pkgdir} install
  rm ${pkgdir}/usr/lib/libgcc_s.so*
  
  make -C $CHOST/libstdc++-v3/src DESTDIR=${pkgdir} install
  make -C $CHOST/libstdc++-v3/include DESTDIR=${pkgdir} install
  make -C $CHOST/libstdc++-v3/libsupc++ DESTDIR=${pkgdir} install
  make -C $CHOST/libstdc++-v3/python DESTDIR=${pkgdir} install

  make DESTDIR=${pkgdir} install-libcc1
  install -d $pkgdir/usr/share/gdb/auto-load/usr/lib
  mv $pkgdir/usr/lib/libstdc++.so.6.*-gdb.py \
    $pkgdir/usr/share/gdb/auto-load/usr/lib/
  rm ${pkgdir}/usr/lib/libstdc++.so*

  make DESTDIR=${pkgdir} install-fixincludes
  make -C gcc DESTDIR=${pkgdir} install-mkheaders
  
  make -C lto-plugin DESTDIR=${pkgdir} install
  install -dm755 ${pkgdir}/usr/lib/bfd-plugins/
  ln -s /usr/libexec/gcc/$CHOST/${pkgver}/liblto_plugin.so \
    ${pkgdir}/usr/lib/bfd-plugins/

  make -C $CHOST/libgomp DESTDIR=${pkgdir} install-nodist_toolexeclibHEADERS \
    install-nodist_libsubincludeHEADERS
  make -C $CHOST/libitm DESTDIR=${pkgdir} install-nodist_toolexeclibHEADERS
  make -C $CHOST/libquadmath DESTDIR=${pkgdir} install-nodist_libsubincludeHEADERS
  make -C $CHOST/libsanitizer DESTDIR=${pkgdir} install-nodist_{saninclude,toolexeclib}HEADERS
  make -C $CHOST/libsanitizer/asan DESTDIR=${pkgdir} install-nodist_toolexeclibHEADERS
  make -C $CHOST/libmpx DESTDIR=${pkgdir} install-nodist_toolexeclibHEADERS

  make -C libiberty DESTDIR=${pkgdir} install
  # install PIC version of libiberty
  install -m644 ${srcdir}/gcc-build/libiberty/pic/libiberty.a ${pkgdir}/usr/lib

  make -C gcc DESTDIR=${pkgdir} install-man install-info
  rm ${pkgdir}/usr/share/man/man1/{gccgo,gfortran}.1
  rm ${pkgdir}/usr/share/info/{gccgo,gfortran,gnat-style,gnat_rm,gnat_ugn}.info

  make -C libcpp DESTDIR=${pkgdir} install
  make -C gcc DESTDIR=${pkgdir} install-po

  # many packages expect this symlink
  ln -s gcc ${pkgdir}/usr/bin/cc

  # POSIX conformance launcher scripts for c89 and c99
  cat > $pkgdir/usr/bin/c89 <<"EOF"
#!/bin/sh
fl="-std=c89"
for opt; do
  case "$opt" in
    -ansi|-std=c89|-std=iso9899:1990) fl="";;
    -std=*) echo "`basename $0` called with non ANSI/ISO C option $opt" >&2
	    exit 1;;
  esac
done
exec gcc $fl ${1+"$@"}
EOF

  cat > $pkgdir/usr/bin/c99 <<"EOF"
#!/bin/sh
fl="-std=c99"
for opt; do
  case "$opt" in
    -std=c99|-std=iso9899:1999) fl="";;
    -std=*) echo "`basename $0` called with non ISO C99 option $opt" >&2
	    exit 1;;
  esac
done
exec gcc $fl ${1+"$@"}
EOF

  chmod 755 $pkgdir/usr/bin/c{8,9}9

  # install the libstdc++ man pages
  make -C $CHOST/libstdc++-v3/doc DESTDIR=$pkgdir doc-install-man

  # Add licenses
  install -m755 -d ${pkgdir}/usr/share/licenses/gcc
  install -m644 ${srcdir}/gcc-$pkgver/COPYING{3{,.LIB},.RUNTIME} ${srcdir}/COPYING.DOC \
    ${pkgdir}/usr/share/licenses/gcc
}

package_gcc-fortran()
{
  pkgdesc="Fortran front-end for GCC"
  depends=("gcc=$pkgver-$pkgrel")
  options=('!emptydirs')

  cd ${srcdir}/gcc-build
  make -C $CHOST/libgfortran DESTDIR=$pkgdir install-cafexeclibLTLIBRARIES \
    install-{toolexeclibDATA,nodist_fincludeHEADERS}
  make -C $CHOST/libgomp DESTDIR=$pkgdir install-nodist_fincludeHEADERS
  make -C gcc DESTDIR=$pkgdir fortran.install-{common,man,info}
  install -Dm755 gcc/f951 $pkgdir/${_libdir}/f951

  ln -s gfortran ${pkgdir}/usr/bin/f95

  # Add licenses
  install -m755 -d ${pkgdir}/usr/share/licenses/gcc-fortran
  install -m644 ${srcdir}/gcc-$pkgver/COPYING{3{,.LIB},.RUNTIME} ${srcdir}/COPYING.DOC \
    ${pkgdir}/usr/share/licenses/gcc-fortran
}

package_gcc-objc()
{
  pkgdesc="Objective-C front-end for GCC"
  depends=("gcc=$pkgver-$pkgrel")

  cd ${srcdir}/gcc-build
  make DESTDIR=$pkgdir -C $CHOST/libobjc install-headers
  install -dm755 $pkgdir/${_libdir}
  install -m755 gcc/cc1obj{,plus} $pkgdir/${_libdir}/

  # Add licenses
  install -m755 -d ${pkgdir}/usr/share/licenses/gcc-objc
  install -m644 ${srcdir}/gcc-$pkgver/COPYING{3{,.LIB},.RUNTIME} ${srcdir}/COPYING.DOC \
    ${pkgdir}/usr/share/licenses/gcc-objc
}

package_gcc-ada()
{
  pkgdesc="Ada front-end for GCC (GNAT)"
  depends=("gcc=$pkgver-$pkgrel")
  options=('staticlibs' '!emptydirs')

  cd ${srcdir}/gcc-build/gcc
  make DESTDIR=$pkgdir ada.install-{common,info}
  install -m755 gnat1 $pkgdir/${_libdir}

  ln -s gcc ${pkgdir}/usr/bin/gnatgcc

  # insist on dynamic linking, but keep static libraries because gnatmake complains
  mv ${pkgdir}/${_libdir}/adalib/libgna{rl,t}-${_pkgver}.so ${pkgdir}/usr/lib
  ln -s libgnarl-${_pkgver}.so ${pkgdir}/usr/lib/libgnarl.so
  ln -s libgnat-${_pkgver}.so ${pkgdir}/usr/lib/libgnat.so
  rm ${pkgdir}/${_libdir}/adalib/libgna{rl,t}.so

  # Add licenses
  install -m755 -d ${pkgdir}/usr/share/licenses/gcc-ada
  install -m644 ${srcdir}/gcc-$pkgver/COPYING{3{,.LIB},.RUNTIME} ${srcdir}/COPYING.DOC \
    ${pkgdir}/usr/share/licenses/gcc-ada
}

package_gcc-go()
{
  pkgdesc="Go front-end for GCC"
  depends=("gcc=$pkgver-$pkgrel")
  conflicts=('go')
  options=('!emptydirs')

  cd ${srcdir}/gcc-build
  make -C $CHOST/libgo DESTDIR=$pkgdir install-exec-am
  rm ${pkgdir}/usr/lib/libgo.so*
  make -C gcc DESTDIR=$pkgdir go.install-{common,man,info}
  install -Dm755 gcc/go1 $pkgdir/${_libdir}/go1

  make DESTDIR=${pkgdir} install-gotools

  # Add licenses
  install -m755 -d ${pkgdir}/usr/share/licenses/gcc-go
  install -m644 ${srcdir}/gcc-$pkgver/COPYING{3{,.LIB},.RUNTIME} ${srcdir}/COPYING.DOC \
    ${pkgdir}/usr/share/licenses/gcc-go
}
