# Maintainer (Arch): Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>
# Contributor (Arch): Pierre Schmitz <pierre@archlinux.de>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Maintainer: Márcio Silva <coadde@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=ca-certificates
pkgver=20210119
pkgrel=1
pkgdesc="Common CA certificates"
url="https://src.fedoraproject.org/rpms/$pkgname"
arch=(any)
license=(GPL-2)
depends=('sh' "$pkgname-mozilla" 'coreutils' 'findutils' 'p11-kit>=0.23.21')
makedepends=(asciidoc p11-kit)
provides=($pkgname-utils $pkgname-java)
conflicts=($pkgname-utils $pkgname-java "$pkgname-cacert<=20140824-4")
replaces=($pkgname-utils $pkgname-java "$pkgname-cacert<=20140824-4")
source=(update-ca-trust update-ca-trust.8.txt update-ca-trust.hook COPYING)
sha512sums=('d16acbe01d545053f5629157d1fb1bb8253f508d6f80b17be009970c80c0ddb7055ea6dad304efeb0f8b24669b7e3c4c622ba22ff938467783c4ea5ea86c7244'
            '576287b228b558bce475c45a8cedd50b3ba310e38a49209e3c9aed6156c92ed29d2f44de2a29ba3f55a35f62d9f3958038b6d7005e689f400092c7a87087f2dd'
            'a20e8753bfdf77b374122c9d0a57f1f36285817408c98c86e7caf64aaa2123da46d793c2815a8f681535656e3017ddc43b1d8d6a47d412589e30a3949ab8b6f1'
            'aee80b1f9f7f4a8a00dcf6e6ce6c41988dcaedc4de19d9d04460cbfb05d99829ffe8f9d038468eabbfba4d65b38e8dbef5ecf5eb8a1b891d9839cda6c48ee957')

build() {
  asciidoc.py -v -d manpage -b docbook update-ca-trust.8.txt
  xsltproc --nonet -o update-ca-trust.8 /etc/asciidoc/docbook-xsl/manpage.xsl update-ca-trust.8.xml
}

package() {
  install -D update-ca-trust "$pkgdir/usr/sbin/update-ca-trust"
  install -Dm644 update-ca-trust.8 "$pkgdir/usr/share/man/man8/update-ca-trust.8"
  install -Dm644 update-ca-trust.hook "$pkgdir/usr/share/libalpm/hooks/update-ca-trust.hook"

  # Trust source directories
  install -d "$pkgdir"/{etc,usr/share}/$pkgbase/trust-source/{anchors,blacklist}

  # Directories used by update-ca-trust (aka "trust extract-compat")
  install -d "$pkgdir"/etc/{ssl/certs/{edk2,java},$pkgbase/extracted}

  # Compatibility link for LibreSSL using /etc/ssl as CAdir
  # Used in preference to the individual links in /etc/ssl/certs
  ln -sr "$pkgdir/etc/$pkgbase/extracted/tls-ca-bundle.pem" "$pkgdir/etc/ssl/cert.pem"

  # Compatibility link for legacy bundle
  ln -sr "$pkgdir/etc/$pkgbase/extracted/tls-ca-bundle.pem" "$pkgdir/etc/ssl/certs/ca-certificates.crt"

  # License
  install -Dm644 COPYING "$pkgdir/usr/share/licenses/$pkgname/COPYING"
}