# Maintainer (Arch): Dave Reisner <dreisner@archlinux.org>
# Maintainer (Arch): Andreas Radke <andyrtr@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Jesús E. <heckyel@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>

pkgname=ed
pkgver=1.16
pkgrel=2
pkgdesc="A POSIX-compliant line-oriented text editor"
arch=('i686' 'x86_64')
license=('GPL-3')
url="https://www.gnu.org/software/ed/ed.html"
depends=('sh')
options=('!emptydirs')
validpgpkeys=('1D41C14B272A2219A739FA4F8FE99503132D7742')  # Antonio Diaz Diaz
source=("https://ftp.gnu.org/gnu/ed/$pkgname-$pkgver.tar.lz"{,.sig})
sha512sums=('1ca999edd7007c56bf9aa91436997a813e665910dda19a3307fa5b85adc05667eb120cb54cb6544b919f1c7f631baf3ee03079abe1171b875fb9653f535fa7bd'
            'SKIP')

build() {
  cd "$pkgname-$pkgver"

  ./configure --prefix=/ \
              --mandir=/usr/share/man \
              --infodir=/usr/share/info \
              "CPPFLAGS=$CPPFLAGS" \
              "CFLAGS=$CFLAGS" \
              "LDFLAGS=$LDFLAGS"

  make
}

check() {
  make -C "$pkgname-$pkgver" check
}

package() {
  make -C "$pkgname-$pkgver" "DESTDIR=$pkgdir" install
  install -Dm644 "$pkgname-$pkgver"/COPYING $pkgdir/usr/share/licenses/$pkgname/COPYING
}

# vim:set ts=2 sw=2 et:
