# Maintainer (Arch): Christian Hesse <mail@eworm.de>
# Maintainer (Arch): Ronald van Haren <ronald.archlinux.org>
# Contributor (Arch): Tobias Powalowski <tpowa@archlinux.org>
# Contributor (Arch): Keshav Amburay
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Maintainer: Márcio Silva <coadde@hyperbola.info>
# Contributor: rachad <rachad@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

## '1' to enable Xen support, '0' to disable
_XEN='1'

## '1' to enable IA32-EFI build in Hyperbola x86_64, '0' to disable
_IA32_EFI_IN_ARCH_X64='1'

## '1' to enable IA32-XEN build in Hyperbola x86_64, "0" to disable
_IA32_XEN_IN_ARCH_X64='1'

## '1' to enable EMU build (x86_64 and i686 only), "0" to disable
_GRUB_EMU_BUILD='0'

_UNIFONT_VER='13.0.06'

[[ "${CARCH}" = 'x86_64' ]] && _EFI_ARCH='x86_64'
[[ "${CARCH}" = 'i686' ]] && _EFI_ARCH='i386'

[[ "${CARCH}" = 'x86_64' ]] && _XEN_ARCH='x86_64'

[[ "${CARCH}" = 'x86_64' ]] && _EMU_ARCH='x86_64'
[[ "${CARCH}" = 'i686' ]] && _EMU_ARCH='i386'

pkgname=grub
pkgdesc='GNU GRand Unified Bootloader'
pkgver=2.04
_debver=$pkgver
_debrel=20
pkgrel=7
epoch=2
url='https://www.gnu.org/software/grub/'
arch=('i686' 'x86_64')
license=('GPL-3' 'Expat' 'CC-BY-SA-3.0')
backup=('etc/default/grub'
        'etc/grub.d/40_custom')
install="${pkgname}.install"
options=('!makeflags')
depends=('device-mapper' 'sh' 'efivar')
makedepends=('autogen' 'efivar' 'freetype2' 'fuse2' 'help2man' 'python' 'quilt' 'rsync' 'ttf-dejavu')

if [[ "${CARCH}" = 'x86_64' ]] && [[ "${_XEN}" = '1' ]]; then
	makedepends+=('xen')
	optdepends+=('xen: For grub Xen support')
fi

if [[ "${_GRUB_EMU_BUILD}" = '1' ]]; then
	makedepends+=('libusbx' 'sdl')
	optdepends+=('libusbx: For grub-emu USB support'
	             'sdl: For grub-emu SDL support')
fi

optdepends=('freetype2: For grub-mkfont usage'
            'fuse2: For grub-mount usage')

if [[ "${CARCH}" = 'x86_64' ]] || [[ "${CARCH}" = 'i686' ]]; then
	provides=('grub-common' 'grub-bios' 'grub-emu' "grub-efi-${_EFI_ARCH}")
	conflicts=('grub-common' 'grub-bios' 'grub-emu' "grub-efi-${_EFI_ARCH}" 'grub-legacy')
	replaces=('grub-common' 'grub-bios' 'grub-emu' "grub-efi-${_EFI_ARCH}")
fi

source=("https://ftp.gnu.org/gnu/${pkgname}/${pkgname}-${pkgver}.tar.xz"{,.sig}
        "https://repo.hyperbola.info:50000/sources/grub/grub-extras.tar.gz"{,.sig}
        "https://repo.hyperbola.info:50000/sources/grub/gnulib.tar.gz"{,.sig}
        "https://repo.hyperbola.info:50000/sources/grub/grub2_${_debver}-${_debrel}.debian.tar.xz"{,.sig}
        "https://ftp.gnu.org/gnu/unifont/unifont-${_UNIFONT_VER}/unifont-${_UNIFONT_VER}.bdf.gz"{,.sig}
        '0001-10_linux-20_linux_xen-detect-hyperbola-initramfs.patch'
        '0002-add-GRUB_COLOR_variables.patch'
        '0003-10_linux-20_linux_xen-rebrand-free-distros.patch'
        '0004-20_linux_xen-detect-kernel-configuration-files.patch'
        '0005-20_linux_xen-remove-placeholder-entry.patch'
        'grub.default'
        'remove-relocator64.patch')

sha512sums=('9c15c42d0cf5d61446b752194e3b628bb04be0fe6ea0240ab62b3d753784712744846e1f7c3651d8e0968d22012e6d713c38c44936d4004ded3ca4d4007babbb'
            'SKIP'
            'ba34812007ab2acac08ae7bcf7f9168473657704d65a82ece28a93666cdf1b8d49f5f4aa67d4edc1d8bbf498dc0e2f3adb0e1164f3c9af1bfe0a955043bf4fa3'
            'SKIP'
            '8012a5472de910bd4d6637a7a889d81bfc12cfbd00adeb6a12d4aad43e8d8a90884fc9ecfcaef43faf9b7030f14b0071b57d71f1c87dab1d9cfe94371d4bb5e9'
            'SKIP'
            '61d6900140f83a5e4c066e3f530d5093cd1961b036f37c29eefd14131fb2e2022602dcfff2ac6cd58769bca1cb824446550b0c2a9aba6b4a3ae5c7ed82943b5a'
            'SKIP'
            '45cf6a0c08d498a41a0bf2acff1ba34c1a59991f9f4a7b73c2124b408ce17b5aece8a008b85a196e84e031448c0333bc1fdaa65ad6329f1c0889ef56a218ae27'
            'SKIP'
            '3529ab4d08a4165e081c49dfc1cf40079ad9a1b9dd7ab6d39147fc347cb6aa615fd90292ecd5ecbfb543bf444bef27043a2392029d0210f9b4a6369365d3da1b'
            '0ae2f50a397268ea0ff46faa180e699ba956acaa68504d9dde7c33ab194430df57c2e2e5f9fe30b6c31e7806666faad4b274747ba151035e338bcaab3d875c3e'
            'f88b937b9a4f24cc1f2a113c55a8ab01b8b610616e72d2191f375d32b2ae27ca6a50811632711848253026e0c9a36a40f8be8bec1daa33f03666b00f60f1b14b'
            '63938fde65f20a616a1611f2b533ae596a2e13ca7b30796071bc59762cde0fe588fcc5008bfa593f0fb01e0f929642eeecd687072fdb6aa0d0456d0e8778ceba'
            '22b0b479b1cc448a240739b0be164dc202bfb9688f8219aef5c711a1b96aaf06da78323a5e79d74adefd34ac42fe6566592bf398c9de99f6f35fcd28794d1418'
            'c715b089ba869ed957450403bb2c57b5650a0bb0f6a8e823893e030296268a53870825ea37e219ab3e7261c79e6efacab9fe624a70ecce4c3c4456c2cf54e816'
            'fa7ccf679d69e0ebe2b7b537e566b11122f8dc6d8de85f952e4eec19ac724c63251b40660446d8141be014ac9cd8d6d1da05453c44f9869c1e69c3d1f7e30d4a')

validpgpkeys=('E53D497F3FA42AD8C9B4D1E835A93B74E82E4209'  # Vladimir 'phcoder' Serbinenko <phcoder@gmail.com>
              'BE5C23209ACDDACEB20DB0A28C8189F1988C2166'  # Daniel Kiper <dkiper@net-space.pl>
              '95D2E9AB8740D8046387FD151A09227B1F435A33'  # Paul Hardy <unifoundry@unifoundry.com>
              'C92BAA713B8D53D3CAE63FC9E6974752F9704456') # André Silva

prepare() {
	cd "${srcdir}/grub-${pkgver}/"

	if [[ $pkgver = $_debver ]]; then
	  export QUILT_PATCHES=debian/patches
	  export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
	  export QUILT_DIFF_ARGS='--no-timestamps'

	  mv ../debian .

	  # Doesn't apply
	  rm -v debian/patches/2021-02-security/099-kern-efi-Add-initial-stack-protector-implementation.patch || true
	  rm -v debian/patches/dejavu-font-path.patch || true
	  rm -v debian/patches/dpkg-version-comparison.patch || true
	  rm -v debian/patches/install-efi-adjust-distributor.patch || true
	  rm -v debian/patches/maybe-quiet.patch || true
	  rm -v debian/patches/mkconfig-other-inits.patch || true
	  rm -v debian/patches/mkconfig-recovery-title.patch || true
	  rm -v debian/patches/mkconfig-ubuntu-distributor.patch || true
	  rm -v debian/patches/mkconfig-ubuntu-recovery.patch || true
	  rm -v debian/patches/quick-boot.patch || true
	  rm -v debian/patches/quick-boot-lvm.patch || true
	  rm -v debian/patches/gfxpayload-dynamic.patch || true
	  rm -v debian/patches/vt-handoff.patch || true
	  [[ "${CARCH}" = 'i686' ]] && rm -v debian/patches/relocator-chunk-align-underflow.patch || true

	  quilt push -av
	fi

	# Patch to detect of Hyperbola GNU/Linux-libre initramfs images by grub-mkconfig
	patch -Np1 -i "${srcdir}/0001-10_linux-20_linux_xen-detect-hyperbola-initramfs.patch"

	# Patch to enable GRUB_COLOR_* variables in grub-mkconfig
	# Based on http://lists.gnu.org/archive/html/grub-devel/2012-02/msg00021.html
	patch -Np1 -i "${srcdir}/0002-add-GRUB_COLOR_variables.patch"

	# Fix DejaVuSans.ttf location so that grub-mkfont can create *.pf2 files for starfield theme
	sed 's|/usr/share/fonts/dejavu|/usr/share/fonts/dejavu /usr/share/fonts/TTF|g' -i "configure.ac"

	# Rebranding for some free operating-systems
	patch -Np1 -i "${srcdir}/0003-10_linux-20_linux_xen-rebrand-free-distros.patch"

	# Patch to detect kernel configuration files, required for Xen kernels
	patch -Np1 -i "${srcdir}/0004-20_linux_xen-detect-kernel-configuration-files.patch"

	# Remove the "placeholder" entry in 20_linux_xen.in
	patch -Np1 -i "${srcdir}/0005-20_linux_xen-remove-placeholder-entry.patch"

	if [[ "${CARCH}" = 'i686' ]]; then
		# Remove relocator64 assembly code, only useful with x86_64 architecture
		patch -Np1 -i "${srcdir}/remove-relocator64.patch"
	fi

	# Fix mkinitcpio 'rw'
	sed 's| ro | rw |g' -i "util/grub.d/10_linux.in"

	# Pull in latest language files
	./linguas.sh

	# Remove not working langs which need LC_ALL=C.UTF-8
	# Remove langs autogenerated in ./linguas.sh
	sed -e 's#de_CH##g' -i "po/LINGUAS"
	sed -e 's#de@hebrew##g' -i "po/LINGUAS"
	sed -e 's#en@arabic##g' -i "po/LINGUAS"
	sed -e 's#en@cyrillic##g' -i "po/LINGUAS"
	sed -e 's#en@greek##g' -i "po/LINGUAS"
	sed -e 's#en@hebrew##g' -i "po/LINGUAS"
	sed -e 's#en@piglatin##g' -i "po/LINGUAS"
	sed -e 's#en@quot##g' -i "po/LINGUAS"

	# Avoid problem with unifont during compile of grub, http://savannah.gnu.org/bugs/?40330
	cp "${srcdir}/unifont-${_UNIFONT_VER}.bdf" "unifont.bdf"

	# Add the grub-extra sources for BIOS build
	install -d "${srcdir}/grub-${pkgver}/grub-extras"
	cp -r "${srcdir}/grub-extras/915resolution" "${srcdir}/grub-${pkgver}/grub-extras/915resolution"
	export GRUB_CONTRIB="${srcdir}/grub-${pkgver}/grub-extras/"

	./autogen.sh
}

_configure_options=(
	FREETYPE="pkg-config freetype2"
	BUILD_FREETYPE="pkg-config freetype2"
	--enable-mm-debug
	--enable-nls
	--enable-device-mapper
	--enable-cache-stats
	--enable-grub-mkfont
	--enable-grub-mount
	--prefix="/usr"
	--bindir="/usr/bin"
	--sbindir="/usr/sbin"
	--mandir="/usr/share/man"
	--infodir="/usr/share/info"
	--datarootdir="/usr/share"
	--sysconfdir="/etc"
	--program-prefix=""
	--with-bootdir="/boot"
	--with-grubdir="grub"
	--disable-silent-rules
	--disable-werror
)

_build_grub-efi() {
	cp -r "${srcdir}/grub-${pkgver}" "${srcdir}/grub-${pkgver}-efi-${_EFI_ARCH}"

	unset CFLAGS
	unset CPPFLAGS
	unset CXXFLAGS
	unset LDFLAGS
	unset MAKEFLAGS

	cd "${srcdir}/grub-${pkgver}-efi-${_EFI_ARCH}"

	./configure \
		--with-platform="efi" \
		--target="${_EFI_ARCH}" \
		--disable-efiemu \
		--enable-boot-time \
		"${_configure_options[@]}"

	make
}

_build_grub-xen() {
	cp -r "${srcdir}/grub-${pkgver}" "${srcdir}/grub-${pkgver}-xen-${_XEN_ARCH}"

	unset CFLAGS
	unset CPPFLAGS
	unset CXXFLAGS
	unset LDFLAGS
	unset MAKEFLAGS

	cd "${srcdir}/grub-${pkgver}-xen-${_XEN_ARCH}"

	./configure \
		--with-platform='xen' \
		--target="${_XEN_ARCH}" \
		--disable-efiemu \
		--enable-boot-time \
		"${_configure_options[@]}"

	make
}

_build_grub-bios() {
	if [[ "${CARCH}" = 'x86_64' ]]; then
		_EFIEMU='--enable-efiemu'
	else
		_EFIEMU='--disable-efiemu'
	fi

	cp -r "${srcdir}/grub-${pkgver}" "${srcdir}/grub-${pkgver}-bios"

	unset CFLAGS
	unset CPPFLAGS
	unset CXXFLAGS
	unset LDFLAGS
	unset MAKEFLAGS

	cd "${srcdir}/grub-${pkgver}-bios"

	./configure \
		--with-platform='pc' \
		--target='i386' \
		"${_EFIEMU}" \
		--enable-boot-time \
		"${_configure_options[@]}"

	make
}

_build_grub-qemu() {
	cp -r "${srcdir}/grub-${pkgver}" "${srcdir}/grub-${pkgver}-qemu"

	unset CFLAGS
	unset CPPFLAGS
	unset CXXFLAGS
	unset LDFLAGS
	unset MAKEFLAGS

	cd "${srcdir}/grub-${pkgver}-qemu"

	./configure \
		--with-platform='qemu' \
		--target='i386' \
		--disable-efiemu \
		--enable-boot-time \
		"${_configure_options[@]}"

	make
}

_build_grub-ieee1275() {
	cp -r "${srcdir}/grub-${pkgver}" "${srcdir}/grub-${pkgver}-ieee1275"

	unset CFLAGS
	unset CPPFLAGS
	unset CXXFLAGS
	unset LDFLAGS
	unset MAKEFLAGS

	cd "${srcdir}/grub-${pkgver}-ieee1275"

	./configure \
		--with-platform='ieee1275' \
		--target='i386' \
		--disable-efiemu \
		--enable-boot-time \
		"${_configure_options[@]}"

	make
}

_build_grub-libreboot() {
	cp -r "${srcdir}/grub-${pkgver}" "${srcdir}/grub-${pkgver}-libreboot"

	unset CFLAGS
	unset CPPFLAGS
	unset CXXFLAGS
	unset LDFLAGS
	unset MAKEFLAGS

	cd "${srcdir}/grub-${pkgver}-libreboot"

	./configure \
		--with-platform='coreboot' \
		--target='i386' \
		--disable-efiemu \
		--enable-boot-time \
		"${_configure_options[@]}"

	make
}

_build_grub-multiboot() {
	cp -r "${srcdir}/grub-${pkgver}" "${srcdir}/grub-${pkgver}-multiboot"

	unset CFLAGS
	unset CPPFLAGS
	unset CXXFLAGS
	unset LDFLAGS
	unset MAKEFLAGS

	cd "${srcdir}/grub-${pkgver}-multiboot"

	./configure \
		--with-platform='multiboot' \
		--target='i386' \
		--disable-efiemu \
		--enable-boot-time \
		"${_configure_options[@]}"

	make
}

_build_grub-emu() {
	cp -r "${srcdir}/grub-${pkgver}/" "${srcdir}/grub-${pkgver}-emu/"

	unset CFLAGS
	unset CPPFLAGS
	unset CXXFLAGS
	unset LDFLAGS
	unset MAKEFLAGS

	cd "${srcdir}/grub-${pkgver}-emu"

	./configure \
		--with-platform='emu' \
		--target="${_EMU_ARCH}" \
		--enable-grub-emu-usb=no \
		--enable-grub-emu-sdl=no \
		--disable-grub-emu-pci \
		"${_configure_options[@]}"

	make
}

build() {
	cd "${srcdir}/grub-${pkgver}/"

	if [[ "${CARCH}" = 'x86_64' ]] || [[ "${CARCH}" = 'i686' ]]; then
		_build_grub-efi

		if [[ "${CARCH}" = 'x86_64' ]] && [[ "${_IA32_EFI_IN_ARCH_X64}" = '1' ]]; then
			_EFI_ARCH='i386' _build_grub-efi
		fi
	fi

	if [[ "${CARCH}" = 'x86_64' ]] || [[ "${CARCH}" = 'i686' ]]; then
		if [[ "${CARCH}" = 'x86_64' ]] && [[ "${_XEN}" = '1' ]]; then
			_build_grub-xen

			if [[ "${_IA32_XEN_IN_ARCH_X64}" = '1' ]]; then
				_XEN_ARCH='i386' _build_grub-xen
			fi
		fi

		_build_grub-bios

		_build_grub-qemu

		_build_grub-ieee1275

		_build_grub-libreboot

		_build_grub-multiboot
	fi

	if [[ "${_GRUB_EMU_BUILD}" = '1' ]]; then
		_build_grub-emu
	fi
}

_package_grub-efi() {
	cd "${srcdir}/grub-${pkgver}-efi-${_EFI_ARCH}/"

	make DESTDIR="${pkgdir}/" bashcompletiondir="/usr/share/bash-completion/completions" install

	rm -f "${pkgdir}/usr/lib/grub/${_EFI_ARCH}-efi"/*.module || true
	rm -f "${pkgdir}/usr/lib/grub/${_EFI_ARCH}-efi"/*.image || true
	rm -f "${pkgdir}/usr/lib/grub/${_EFI_ARCH}-efi"/{kernel.exec,gdb_grub,gmodule.pl} || true
}

_package_grub-xen() {
	cd "${srcdir}/grub-${pkgver}-xen-${_XEN_ARCH}/"

	make DESTDIR="${pkgdir}/" bashcompletiondir="/usr/share/bash-completion/completions" install

	rm -f "${pkgdir}/usr/lib/grub/${_XEN_ARCH}-xen"/*.module || true
	rm -f "${pkgdir}/usr/lib/grub/${_XEN_ARCH}-xen"/*.image || true
	rm -f "${pkgdir}/usr/lib/grub/${_XEN_ARCH}-xen"/{kernel.exec,gdb_grub,gmodule.pl} || true
}

_package_grub-bios() {
	cd "${srcdir}/grub-${pkgver}-bios/"

	make DESTDIR="${pkgdir}/" bashcompletiondir="/usr/share/bash-completion/completions" install

	rm -f "${pkgdir}/usr/lib/grub/i386-pc"/*.module || true
	rm -f "${pkgdir}/usr/lib/grub/i386-pc"/*.image || true
	rm -f "${pkgdir}/usr/lib/grub/i386-pc"/{kernel.exec,gdb_grub,gmodule.pl} || true
}

_package_grub-qemu() {
	cd "${srcdir}/grub-${pkgver}-qemu/"

	make DESTDIR="${pkgdir}/" bashcompletiondir="/usr/share/bash-completion/completions" install

	rm -f "${pkgdir}/usr/lib/grub/i386-qemu"/*.module || true
	rm -f "${pkgdir}/usr/lib/grub/i386-qemu"/*.image || true
	rm -f "${pkgdir}/usr/lib/grub/i386-qemu"/{kernel.exec,gdb_grub,gmodule.pl} || true
}

_package_grub-ieee1275() {
	cd "${srcdir}/grub-${pkgver}-ieee1275/"

	make DESTDIR="${pkgdir}/" bashcompletiondir="/usr/share/bash-completion/completions" install

	rm -f "${pkgdir}/usr/lib/grub/i386-ieee1275"/*.module || true
	rm -f "${pkgdir}/usr/lib/grub/i386-ieee1275"/*.image || true
	rm -f "${pkgdir}/usr/lib/grub/i386-ieee1275"/{kernel.exec,gdb_grub,gmodule.pl} || true
}

_package_grub-libreboot() {
	cd "${srcdir}/grub-${pkgver}-libreboot/"

	make DESTDIR="${pkgdir}/" bashcompletiondir="/usr/share/bash-completion/completions" install

	rm -f "${pkgdir}/usr/lib/grub/i386-coreboot"/*.module || true
	rm -f "${pkgdir}/usr/lib/grub/i386-coreboot"/*.image || true
	rm -f "${pkgdir}/usr/lib/grub/i386-coreboot"/{kernel.exec,gdb_grub,gmodule.pl} || true
}

_package_grub-multiboot() {
	cd "${srcdir}/grub-${pkgver}-multiboot/"

	make DESTDIR="${pkgdir}/" bashcompletiondir="/usr/share/bash-completion/completions" install

	rm -f "${pkgdir}/usr/lib/grub/i386-multiboot"/*.module || true
	rm -f "${pkgdir}/usr/lib/grub/i386-multiboot"/*.image || true
	rm -f "${pkgdir}/usr/lib/grub/i386-multiboot"/{kernel.exec,gdb_grub,gmodule.pl} || true
}

_package_grub-emu() {
	cd "${srcdir}/grub-${pkgver}-emu/"

	make DESTDIR="${pkgdir}/" bashcompletiondir="/usr/share/bash-completion/completions" install

	rm -f "${pkgdir}/usr/lib/grub/${_EMU_ARCH}-emu"/*.module || true
	rm -f "${pkgdir}/usr/lib/grub/${_EMU_ARCH}-emu"/*.image || true
	rm -f "${pkgdir}/usr/lib/grub/${_EMU_ARCH}-emu"/{kernel.exec,gdb_grub,gmodule.pl} || true
}

package() {
	optdepends+=('dosfstools: For grub-mkrescue FAT FS and EFI support'
	             'efibootmgr: For grub-install EFI support'
	             'libisoburn: Provides xorriso for generating grub rescue iso using grub-mkrescue'
	             'os-prober: To detect other OSes when generating grub.cfg in BIOS systems'
	             'mtools: For grub-mkrescue FAT FS and EFI support')

	if [[ "${CARCH}" = 'x86_64' ]]; then
		optdepends+=('xen: For Xen Dom0 support'
		             'xen-docs: For Xen documentation')
	fi

	if [[ "${_GRUB_EMU_BUILD}" = '1' ]]; then
		optdepends+=('libusbx: For grub-emu USB support'
		             'sdl: For grub-emu SDL support')
	fi

	cd "${srcdir}/grub-${pkgver}/"

	if [[ "${CARCH}" = 'x86_64' ]] || [[ "${CARCH}" = 'i686' ]]; then
		_package_grub-efi

		if [[ "${CARCH}" = 'x86_64' ]] && [[ "${_IA32_EFI_IN_ARCH_X64}" = '1' ]]; then
			_EFI_ARCH='i386' _package_grub-efi
		fi
	fi

	if [[ "${CARCH}" = 'x86_64' ]] || [[ "${CARCH}" = 'i686' ]]; then
		if [[ "${CARCH}" = 'x86_64' ]] && [[ "${_XEN}" = '1' ]]; then
			_package_grub-xen

			if [[ "${_IA32_XEN_IN_ARCH_X64}" = '1' ]]; then
				_XEN_ARCH='i386' _package_grub-xen
			fi
		fi

		_package_grub-bios

		_package_grub-qemu

		_package_grub-ieee1275

		_package_grub-libreboot

		_package_grub-multiboot
	fi

	if [[ "${_GRUB_EMU_BUILD}" = '1' ]]; then
		_package_grub-emu
	fi

	install -D -m0644 "${srcdir}/grub.default" "${pkgdir}/etc/default/grub"

	install -D -m0644 "${srcdir}/grub-${pkgver}/COPYING" "${pkgdir}/usr/share/licenses/grub/COPYING"
}
