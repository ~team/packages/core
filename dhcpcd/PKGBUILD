# Maintainer (Arch): Ronald van Haren <ronald.archlinux.org>
# Contributor (Arch): Tom Killian <tom.archlinux.org>
# Contributor (Arch): Judd Vinet <jvinet.zeroflux.org>
# Contributor (Artix): artoo <artoo@cromnix.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=dhcpcd
pkgver=7.1.0
_debver=$pkgver
_debrel=2
pkgrel=3
pkgdesc="RFC2131 compliant DHCP client daemon"
url='https://roy.marples.name/projects/dhcpcd'
arch=('i686' 'x86_64')
license=('Simplified-BSD')
groups=('base')
depends=('sh' 'eudev')
makedepends=('quilt')
optdepends=('openresolv: resolvconf support'
            'logger: message logging support')
provides=('dhcp-client')
backup=('etc/dhcpcd.conf'
        'etc/sv/dhcpcd/conf')
options=('emptydirs')  # We Need the Empty /var/lib/dhcpcd Directory
install=dhcpcd.install
source=("${pkgname}-${pkgver}.tar.xz::https://github.com/NetworkConfiguration/dhcpcd/archive/refs/tags/v${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/d/dhcpcd5/dhcpcd5_${_debver}-${_debrel}.debian.tar.xz"
        "dhcpcd.initd"
        "dhcpcd.conf"
        "dhcpcd.run")
sha512sums=('167c1a461fa05d79e4f56fc401041d57d2086294e678b7b53099790d61bbf4bb6d4ce5e36330dce643194910302d9cea8161dddca0cad38146aff34e9d213a02'
            'c8e3bdfa9d1aa8d899c3c0b5ce19dd13bbf05850639540dd68b08a04e5224c262130119569c1ce5e5248e0e1cb43a0ae7e1d4a789f31e7e323747f9b2b639d3f'
            '6d3220155f2d9ed3e3a00afd378eeb70d435e19804201f8bb35498f1f7f3dfdaeaa2f4a01a18f5e96b457d9c173bc6a206b3e67ebf6d95da7e7b350dcd153fde'
            '00b4f6774fd6f864344bc087ff0fafdd485293527db2a4bef0ca2ba37639383f56977c794687c345b705462c2ad442062205938ec9e5e79647aeba2f9f530537'
            'fb7686be787e13ae4dfbad280102e57a8d9b32531bc7ba655959865d50b9afef13c44006f25f79e8780f7ed1c92f5a2479a3aa4aa9ca392bff9c7eb2ece91540')

prepare() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  # configure variables
  ./configure \
      --prefix=/usr \
      --sysconfdir=/etc \
      --sbindir=/sbin \
      --libdir=/lib \
      --libexecdir=/usr/libexec/dhcpcd \
      --dbdir=/var/lib/dhcpcd \
      --with-hook=ntp.conf \
      --with-hook=wpa_supplicant \
      --rundir=/run

  # Build
  make
}

check() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  make test
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  make DESTDIR="${pkgdir}" install

  # Install License
  install -Dm644 "${srcdir}/${pkgname}-${pkgver}/LICENSE" -t "${pkgdir}/usr/share/licenses/$pkgname"

  # Set Options in /etc/dhcpcd.conf
  echo noipv4ll >> "${pkgdir}/etc/dhcpcd.conf" # Disable ip4vall

  # Set network group permissions in /etc/dhcpcd.conf for dhcpcd-ui
  chgrp network "${pkgdir}/etc/dhcpcd.conf"
  chmod g+w "${pkgdir}/etc/dhcpcd.conf"

  # Install OpenRC init file
  install -Dm755 "${srcdir}/dhcpcd.initd" "${pkgdir}/etc/init.d/dhcpcd"

  # Install runit files
  install -Dm644 "${srcdir}/dhcpcd.conf" "${pkgdir}/etc/sv/dhcpcd/conf"
  install -Dm755 "${srcdir}/dhcpcd.run" "${pkgdir}/etc/sv/dhcpcd/run"
}
