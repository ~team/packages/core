# Maintainer (Arch): Lukas Fleischer <lfleischer@archlinux.org>
# Contributor (Arch): Allan McRae <allan@archlinux.org>
# Contributor (Arch): judd <jvinet@zeroflux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>

pkgname=flex
pkgver=2.6.4
pkgrel=3
pkgdesc="A tool for generating text-scanning programs"
arch=('i686' 'x86_64')
url="https://github.com/westes/flex"
license=('Simplified-BSD')
groups=('base-devel')
depends=('m4')
makedepends=('help2man')
source=("https://github.com/westes/flex/releases/download/v$pkgver/flex-$pkgver.tar.gz"{,.sig}
        'flex-pie.patch')
sha512sums=('e9785f3d620a204b7d20222888917dc065c2036cae28667065bf7862dfa1b25235095a12fd04efdbd09bfd17d3452e6b9ef953a8c1137862ff671c97132a082e'
            'SKIP'
            '54a8b7d39226af49f063934c2fca7a55c4ab8f32a8ac84083f34f31900129653c4f5229f3bc1bc72a3413dd48ae26d3f75e7a1630ab459b41bd93fb0b533199a')
validpgpkeys=('56C67868E93390AA1039AD1CE4B29C8D64885307') # Will Estes

prepare() {
  cd "$pkgname-$pkgver"
  patch -p1 -i "$srcdir"/flex-pie.patch
  autoreconf
}

build() {
  cd "$pkgname-$pkgver"
  ./configure --prefix=/usr
  make
}

check() {
  cd "$pkgname-$pkgver"

  # cxx_restart fails - https://github.com/westes/flex/issues/98
  make -k check || true
}

package() {
  cd "$pkgname-$pkgver"

  make DESTDIR="$pkgdir" install
  ln -s flex "${pkgdir}/usr/bin/lex"

  install -Dm644 COPYING "$pkgdir/usr/share/licenses/$pkgname/COPYING"
}
