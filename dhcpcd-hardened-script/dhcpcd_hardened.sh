#!/bin/sh
# This script wipes hostname and DUID so that DHCP can't send it over the network
echo 'hostname="localhost"' > /etc/conf.d/hostname
# Properly sets our new host in /etc/hosts file
sed -i '/127.0.0.1/c\127.0.0.1       localhost.localdomain   localhost 'localhost'' /etc/hosts
# Avoids need to reboot before showing our new hostname in terminals, etc
hostname localhost
# Delete DUID
rm -rf /etc/dhcpcd.duid
