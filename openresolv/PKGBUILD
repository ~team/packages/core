# Maintainer (Arch): Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>
# Contributor (Arch): Ignacio Galmarino <igalmarino@gmail.com>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Enmanuel E. Saravia <saravia@riseup.net>
# Contributor: Márcio Silva <coadde@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=openresolv
pkgver=3.10.0
pkgrel=3
pkgdesc="resolv.conf management framework (resolvconf)"
arch=('any')
url='https://roy.marples.name/projects/openresolv'
license=('Simplified-BSD')
backup=('etc/resolvconf.conf')
provides=(resolvconf)
depends=('sh')
source=("${pkgname}-${pkgver}.tar.gz::https://github.com/NetworkConfiguration/openresolv/archive/refs/tags/v${pkgver}.tar.gz"
        "dnsmasq.patch")
sha512sums=('756fcb5ff7e0927d6b6a7e384c62e29accaa1192fdec9f99969291bba4936326f63ecdad6976fd543f4946f197a7644060b11bb531cd625b94f87770cfc29d24'
            '6be9c9a59837d30b592c4f9903e63c0eb816b1614e89a2aee870ff23ce5bf12118d101023b0484cd33e2ec2bab77ae5ee9175d0c7a1a4699b1399bdc7ab6239e')

prepare() {
  cd "$pkgname-$pkgver"
  patch -Np1 -i $srcdir/dnsmasq.patch
  sed -n '2,25{s:^# \?::;p}' resolvconf.in >LICENSE
}

build() {
  cd "$pkgname-$pkgver"
  ./configure \
    --prefix=/usr \
    --sysconfdir=/etc \
    --libexecdir=/usr/libexec/resolvconf \
    --sbindir=/sbin
  make
}

package() {
  cd "$pkgname-$pkgver"
  make DESTDIR="$pkgdir" install
  install -Dm644 LICENSE -t "$pkgdir/usr/share/licenses/$pkgname"
}
