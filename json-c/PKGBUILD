# Maintainer (Arch): Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>
# Contributor (Arch): Geoffroy Carrier <geoffroy.carrier@koon.fr>
# Contributor (Arch): congyiwu <congyiwu AT gmail DOT com>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=json-c
pkgver=0.15
_debver=$pkgver
_debrel=2
pkgrel=2
pkgdesc="A JSON implementation in C"
url='https://github.com/json-c/json-c'
license=('Expat')
arch=('i686' 'x86_64')
depends=('glibc')
makedepends=('cmake' 'quilt')
source=("${pkgname}-${pkgver}.tar.gz::https://deb.debian.org/debian/pool/main/j/json-c/json-c_${pkgver}.orig.tar.gz"
        "https://deb.debian.org/debian/pool/main/j/json-c/json-c_${_debver}-${_debrel}+deb11u1.debian.tar.xz")
sha512sums=('35cb3ef403ff5e8905144978ea0a22c9151b63e6bf749a50ca63b3d9320e5018be18aef236490295388d1be2ead7fcf8946d248b28b7ca109a057daaaada2162'
            'b941a5f1fec5d8d72540a3d2512549f8f5df399d346b2db1349a3f1233d145019bd41ff96b682f1f808b32bf1c928bc30b16efa427e657fe543fe0df3d94672e')

prepare() {
  mv "$pkgname-$pkgname-$pkgver-20200726" "$pkgname-$pkgver"
  cd $pkgname-$pkgver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cmake -H$pkgname-$pkgver -Bbuild \
    -DCMAKE_BUILD_TYPE=None \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_INSTALL_LIBDIR=/usr/lib \
    -DENABLE_THREADING=ON \
    -DENABLE_RDRAND=OFF
  cmake --build build
}

package() {
  DESTDIR="$pkgdir" cmake --build build --target install
  install -Dt "$pkgdir/usr/share/licenses/$pkgname" -m644 $pkgname-$pkgver/COPYING
}
