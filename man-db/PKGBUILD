# Maintainer (Arch): Andreas Radke <andyrtr@archlinux.org>
# Contributor (Arch): Sergej Pupykin <sergej@aur.archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>

pkgname=man-db
pkgver=2.9.3
pkgrel=3
pkgdesc="A utility for reading man pages"
arch=('i686' 'x86_64')
url="https://www.nongnu.org/man-db/"
license=('GPL-3')
groups=('base')
depends=('zlib' 'groff' 'libpipeline' 'less' 'libseccomp')
makedepends=('quilt')
optdepends=('gzip' 'cron')
backup=('etc/man_db.conf' 'etc/cron.daily/mandb')
conflicts=('man')
provides=('man')
replaces=('man')
install=${pkgname}.install
source=(https://savannah.nongnu.org/download/man-db/$pkgname-$pkgver.tar.xz{,.asc}
        snapdir.diff
        convert-mans
        mandb.cron.daily
        COPYING
        licensing-info.txt)
validpgpkeys=('AC0A4FF12611B6FCCF01C111393587D97D86500B') # Colin Watson <cjwatson@debian.org>
sha512sums=('ca1c1214753483f6e22efe69a9df9852e0de01a9ad3b9950dcbbc9f38e6060100b98a84333256f8c734002e66e2fd6256bc017a31bd9acfc42002dca2c0f879b'
            'SKIP'
            'f24a8152c82c3b99dab2c34654382512f226bb6b0e5e3b1376d577019a4cca0f4e5a9ac92c62ed7ea5cf0ed3ad94509d34f455d845bc5fb026ef908da82cd5fe'
            '0b159285da20008f0fc0afb21f1eaebd39e8df5b0594880aa0e8a913b656608b8d16bb8d279d9e62d7aae52f62cb9b2fc49e237c6711f4a5170972b38d345535'
            'c1d463a99d5671939faaa15ce7cd27d66f54660ee540fddf44974c0004664e5140273762b52ee7a86fcf36132237b31f9a7eaaf42ddf095f98e1ff2500114c68'
            'd361e5e8201481c6346ee6a886592c51265112be550d5224f1a7a6e116255c2f1ab8788df579d9b8372ed7bfd19bac4b6e70e00b472642966ab5b319b99a2686'
            '0bc3f5811b62dfdff5fa5c3b059248b63ceed3df20b8aead72356c5d2f2cfbdec912cc3e39a067c8b26bc7e8f3bc867b545a4bc3a91b965a06598e0ffb4a80fd')

prepare() {
  cd ${pkgname}-${pkgver}
  # fix default Add MANDB_MAP entry mapping /snap/man to /var/cache/man/snap
  # Hyperbola needs /var/lib/snapd/snap/man to /var/cache/man/snap
  patch -Np0 -i ../snapdir.diff
}

build() {
  cd ${pkgname}-${pkgver}
  ./configure --prefix=/usr \
    --sysconfdir=/etc \
    --with-db=gdbm \
    --disable-setuid \
    --enable-cache-owner=root \
    --enable-mandirs=GNU \
    --without-systemdsystemunitdir \
    --without-systemdtmpfilesdir \
    --with-sections="1 n l 8 3 0 2 5 4 9 6 7"
  make
}

check() {
  cd ${pkgname}-${pkgver}
  make check
}

package() {
  cd ${pkgname}-${pkgver}
  make DESTDIR=${pkgdir} install

  # part of groff pkg
  rm -f ${pkgdir}/usr/bin/zsoelim

  # script from LFS to convert manpages, see
  # http://www.linuxfromscratch.org/lfs/view/6.4/chapter06/man-db.html
  install -D -m755 ${srcdir}/convert-mans  ${pkgdir}/usr/bin/convert-mans

  # install cron job
  install -D -m744 ${srcdir}/mandb.cron.daily ${pkgdir}/etc/cron.daily/mandb

  # install license and licensing info
  for i in COPYING licensing-info.txt; do
    install -Dm644 ${srcdir}/${i} ${pkgdir}/usr/share/licenses/${pkgname}/${i}
  done
}
