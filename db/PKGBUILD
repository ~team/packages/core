# Maintainer (Arch): Stéphane Gaudreault <stephane@archlinux.org>
# Contributor (Arch): Allan McRae <allan@archlinux.org>
# Contributor (Arch): Andreas Radke <andyrtr@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=db
pkgver=5.3.28
_debver=5.3.28+dfsg1
_debrel=0.6
pkgrel=4
pkgdesc="The Berkeley DB embedded database system"
arch=('i686' 'x86_64')
url="https://www.oracle.com/technetwork/database/database-technologies/berkeleydb/overview/index.html"
license=('Modified-BSD')
depends=('gcc-libs' 'sh')
makedepends=('quilt')
source=(https://download.oracle.com/berkeley-db/db-${pkgver}.tar.gz
        https://deb.debian.org/debian/pool/main/d/db5.3/db5.3_$_debver-$_debrel.debian.tar.xz)
sha512sums=('e91bbe550fc147a8be7e69ade86fdb7066453814971b2b0223f7d17712bd029a8eff5b2b6b238042ff6ec1ffa6879d44cb95c5645a922fee305c26c3eeaee090'
            '226241d8b2d364772b566f19c3409208a4a0d01ba67da3a596275eede7e03759e25c3e3d26af4fecb39b92b0ad25115c2eb1665901f8c3863e235a930333fab6')

prepare() {
  cd "${srcdir}"/$pkgname-${pkgver}
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply and seems unimportant
    rm -v debian/patches/002-versioned_symbols.patch || true
    rm -v debian/patches/003-jni_javac.patch || true
    rm -v debian/patches/006-mutex_alignment.patch || true
    rm -v debian/patches/007-link-sql-libs.patch || true
    rm -v debian/patches/008-autoconf-in-lang-sql-sqlite.patch || true

    quilt push -av
  fi
}

build() {
  cd "${srcdir}"/$pkgname-${pkgver}/build_unix
  ../dist/configure --prefix=/usr --enable-compat185 \
    --enable-shared --enable-static --enable-cxx --enable-dbm \
    --enable-stl
  make LIBSO_LIBS=-lpthread
}

package() {
  cd "${srcdir}"/$pkgname-${pkgver}/build_unix
  make DESTDIR="${pkgdir}" install
  rm -r "${pkgdir}"/usr/docs
  install -Dm644 "${srcdir}"/${pkgname}-${pkgver}/LICENSE \
    "${pkgdir}"/usr/share/licenses/${pkgname}/LICENSE
}
