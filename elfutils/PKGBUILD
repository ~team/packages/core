# Maintainer (Arch): Laurent Carlier <lordheavym@gmail.com>
# Contributor (Arch): Stéphane Gaudreault <stephane@archlinux.org>
# Contributor (Arch): Andrej Gelenberg <andrej.gelenberg@udo.edu>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgbase=elfutils
pkgname=(elfutils libelf)
pkgver=0.188
_debver=$pkgver
_debrel=2.1
pkgrel=1
pkgdesc="Utilities to handle ELF object files and DWARF debugging information"
arch=(i686 x86_64)
url='https://sourceware.org/elfutils/'
license=(GPL-3)
depends=(sqlite libarchive curl) # libarchive already have bzip2 as dependency
makedepends=(quilt)
options=(staticlibs)
source=(https://sourceware.org/elfutils/ftp/${pkgver}/elfutils-${pkgver}.tar.bz2
        https://deb.debian.org/debian/pool/main/e/elfutils/elfutils_$_debver-$_debrel.debian.tar.xz)
sha512sums=('585551b2d937d19d1becfc2f28935db1dd1a3d25571a62f322b70ac8da98c1a741a55d070327705df6c3e2ee026652e0b9a3c733b050a0b0ec5f2fc75d5b74b5'
            'e63a4ae7f9246049abf8c75b892e44dafebc4a90efebaf5fa94f8dd8ece4e9a88ce894662d7b2ed046af9068541413d049d7bb3ecde414522bb3edce219a7ba0')

prepare() {
  cd ${pkgbase}-${pkgver}

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/hurd_path.patch || true
    rm -v debian/patches/kfreebsd_path.patch || true
    rm -v debian/patches/riscv-retval-workaround.patch || true

    quilt push -av
  fi
}

build() {
  cd $pkgbase-$pkgver

  CFLAGS+=" -g"  # required for test-suite success
  ./configure --prefix=/usr --program-prefix="eu-" --enable-deterministic-archives --disable-debuginfod

  make
}

package_elfutils() {
  depends=(sqlite libarchive "libelf=$pkgver-$pkgrel")

  cd $pkgbase-$pkgver

  make DESTDIR="$pkgdir" install

  install -Dm644 COPYING $pkgdir/usr/share/licenses/$pkgname/COPYING

  mkdir "$srcdir"/libelf
  mv "$pkgdir"/usr/{lib,include} "$srcdir"/libelf
}

package_libelf() {
  pkgdesc='Libraries to handle ELF object files and DWARF debugging information'
  depends=(bzip2 curl)
  license=(LGPL-3)

  cd $pkgbase-$pkgver

  mv "$srcdir"/libelf "$pkgdir"/usr

  install -Dm644 COPYING-LGPLV3 $pkgdir/usr/share/licenses/$pkgname/COPYING-LGPLV3
}
