# Maintainer (Arch): Allan McRae <allan@archlinux.org>
# Contributor (Arch): judd <jvinet@zeroflux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>

# NOTE: requires rebuilt with each new gcc version

pkgname=libtool
pkgver=2.4.6
_debver=2.4.6
_debrel=14
pkgrel=10
_gccver=8.4.0
pkgdesc="A generic library support script"
arch=('i686' 'x86_64')
url="https://www.gnu.org/software/libtool"
license=('GPL-2')
groups=('base-devel')
depends=('sh' 'tar')
makedepends=("gcc=$_gccver" 'help2man' 'automake=1.15' 'quilt')
provides=("libltdl=$pkgver" "libtool-multilib=$pkgver")
conflicts=('libltdl' 'libtool-multilib')
replaces=('libltdl' 'libtool-multilib')
source=(https://ftp.gnu.org/pub/gnu/libtool/${pkgname}-${pkgver}.tar.xz{,.sig}
        https://deb.debian.org/debian/pool/main/libt/libtool/libtool_$_debver-$_debrel.debian.tar.xz)
sha512sums=('a6eef35f3cbccf2c9e2667f44a476ebc80ab888725eb768e91a3a6c33b8c931afc46eb23efaee76c8696d3e4eed74ab1c71157bcb924f38ee912c8a90a6521a4'
            'SKIP'
            '1cfb4ae9a854ee19e0246fae1ed0d6cac270ce886d8e0003b12df4a740c7323cfdd11795ffc3187b9e0f4d34f03f18b4922f67109274c7e2993ec0e0863c704f')
validpgpkeys=('CFE2BE707B538E8B26757D84151308092983D606')   # Gary Vaughan

prepare() {
  cd $srcdir/$pkgname-$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  cd ${srcdir}/${pkgname}-${pkgver}
  ./configure --prefix=/usr lt_cv_sys_lib_dlsearch_path_spec="/lib /lib32 /usr/lib /usr/lib32 /usr/local/lib /usr/local/lib32"
  make
}

check() {
  cd ${srcdir}/${pkgname}-${pkgver}
  if [[ ${CARCH} = "i686" ]]; then
    # some failures are "expected"
    make check || true
  else
    make check
  fi
}

package() {
  cd ${srcdir}/${pkgname}-${pkgver}
  make DESTDIR=${pkgdir} install
  install -Dm644 COPYING -t ${pkgdir}/usr/share/licenses/${pkgname}
}
