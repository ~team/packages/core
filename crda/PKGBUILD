# Maintainer (Arch): Thomas Bächler <thomas@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>

pkgname=crda
pkgver=4.14+git20191112.9856751
_debver=4.14+git20191112.9856751
_debrel=1
pkgrel=2
pkgdesc="Central Regulatory Domain Agent for wireless networks"
arch=(i686 x86_64)
url="https://wireless.kernel.org/en/developers/Regulatory/CRDA"
license=('ISC')
depends=('wireless-regdb' 'libgcrypt' 'eudev' 'iw')
makedepends=('tauthon-m2crypto' 'quilt')
install=crda.install
source=(https://deb.debian.org/debian/pool/main/c/crda/crda_$pkgver.orig.tar.xz
        https://deb.debian.org/debian/pool/main/c/crda/crda_$_debver-$_debrel.debian.tar.xz
        set-wireless-regdom
        make-crda-fhs-compliant.patch)
sha512sums=('3a33800c26734363dbdef45aafd9dfa1b0ae0ca0bd972f83ae911b645136c972d3e8fdd214d6b41b8b3be23312aa0435297ef46e1d3fdfe30dff128dc3e13958'
            'cbd2fdf90d13e5a2c69dd1d3dfd55d541cbe1541848d6e41e88faf7751973824f32a20e4224b098a5bac90c5bd4359d6cd707e4e20ab6c3fe7a5245493d2b127'
            '8b5dc563c42422bcb96235e3abcfc67bf76c1b839c4abd599e13bbc310514010c2e47f8b0a1966a5a692016a1bf3c3b5b80ef916f5c1263b3e33ee4d9d09c3fa'
            'c49fca9392126e17b816d1c86bb0c62686abaac72c3b74bc8573d69363027ca643527b8541a76d4b3b56f129b8179b5586df654a13e94736d8de9ff2719bc8ea')
validpgpkeys=('E4053F8D0E7C4B9A0A20AB27DC553250F8FE7407') #Luis R. Rodriguez
prepare() {
  cd "${srcdir}"/${pkgname}-${pkgver}
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
  sed 's|^#!/usr/bin/env python|#!/usr/bin/tauthon|' -i utils/key2pub.py
  patch -p1 -i ../make-crda-fhs-compliant.patch
}

build() {
  cd "${srcdir}"/${pkgname}-${pkgver}
  make
}

package() {
  # Install crda, regdbdump and udev rules
  cd "${srcdir}"/${pkgname}-${pkgver}
  make DESTDIR="${pkgdir}" UDEV_RULE_DIR=/lib/udev/rules.d/ install
  # This rule automatically sets the regulatory domain when cfg80211 is loaded
  echo 'ACTION=="add", SUBSYSTEM=="module", DEVPATH=="/module/cfg80211", RUN+="/usr/libexec/crda/set-wireless-regdom"' >> "${pkgdir}"/lib/udev/rules.d/85-regulatory.rules

  install -D -m644 "${srcdir}"/${pkgname}-${pkgver}/LICENSE "${pkgdir}"/usr/share/licenses/crda/LICENSE

  install -D -m755 "${srcdir}"/set-wireless-regdom "${pkgdir}"/usr/libexec/crda/set-wireless-regdom
}
