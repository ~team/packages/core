# Maintainer (Arch): Eric Bélanger <eric@archlinux.org>
# Contributor (Artix): artoo <artoo@cromnix.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>

pkgname=gpm
pkgver=1.20.7
_debver=1.20.7
_debrel=6
pkgrel=10
pkgdesc="A mouse server for the console and xterm"
arch=('i686' 'x86_64')
url="https://www.nico.schottelius.org/software/gpm/"
license=('GPL-2')
depends=('sh')
makedepends=('quilt')
optdepends=('logger: message logging support')
backup=('etc/conf.d/gpm')
options=('!makeflags')
source=(https://www.nico.schottelius.org/software/gpm/archives/${pkgname}-${pkgver}.tar.lzma
        https://deb.debian.org/debian/pool/main/g/gpm/gpm_$_debver-$_debrel.debian.tar.xz
        0001-glibc-sigemptyset.patch
        gpm.sh gpm.confd gpm.initd gpm.run)
sha512sums=('a502741e2f457b47e41c6d155b1f7ef7c95384fd394503f82ddacf80cde9cdc286c906c77be12b6af8565ef1c3ab24d226379c1dcebcfcd15d64bcf3e94b63b9'
            '8e75187fe092764caf4a318dc57c96526a7855fc8db9b5039f8f066c83b09fcd61b1f741942ee0d4138095e506e25c45d445b3aa39caab52c543671108dbacdf'
            '7dae3e570beeca257a2915edc2e1f1065569ba16e5bd83b2bb4a814d43d9dedcecd7b64cb921f32d2016f468fed7cd00150e7d0700c4cad2ce134c5f21068af6'
            '783d20b3811d356312bde6b31f8fdcb3c8cc2f9dcd45e6d9e8088e59cdb7fb3ecf0ed8acf202854d93b653ebb5fd50c80859bbea22e4d3326faf5705cac4b1ef'
            '14a4306a7454d1a12fda29973b51aadd6d1a5cf4833b1be5298cfe0c1ebc63a479cc79490007ae92c6ae24368e19215dfc4bd50ef5b0cfa0fef88cf69db67065'
            '19cfdab0e401ebc3571975acb6687891a33a176ba23fb04537c8724c2dab47617b67e144b4a89c933e6fdd7e2918857e8eadb2bc270e8ec96a84d855810e5934'
            'ea6542f8ce1bb58a6f8eed7dd63018522d05c2333aa4077b78385f4415170102762ebbb4fe1500d671b6289e87e3f5d02768e48ef432fcb492c3ca11aa29c524')

prepare() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply and seems unimportant
    rm -v debian/patches/007_doc_fix_000 || true

    quilt push -av
  else
    patch -Np1 < ../0001-glibc-sigemptyset.patch
  fi
}

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  ./autogen.sh
  ./configure --prefix=/usr --sysconfdir=/etc
  make
}

package() {
  cd "${srcdir}/${pkgname}-${pkgver}"
  make DESTDIR="${pkgdir}" install
  install -D -m755 ../gpm.sh "${pkgdir}/etc/profile.d/gpm.sh"
  install -D -m644 ../gpm.confd "${pkgdir}/etc/conf.d/gpm"
  install -D -m755 ../gpm.initd "${pkgdir}/etc/init.d/gpm"
  install -D -m755 ../gpm.run "${pkgdir}/etc/sv/gpm/run"

  # fix library
  ln -s libgpm.so.2 "${pkgdir}/usr/lib/libgpm.so"
  chmod 755 "${pkgdir}"/usr/lib/libgpm.so.2*

  # install license
  install -D -m644 COPYING "${pkgdir}/usr/share/licenses/${pkgname}/COPYING"
}
