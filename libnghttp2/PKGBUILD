# Maintainer (Arch): Anatol Pomozov
# Contributor (Arch): Zhuoyun Wei <wzyboy@wzyboy.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=libnghttp2
pkgver=1.43.0
_debver=$pkgver
_debrel=1
pkgrel=1
pkgdesc='Framing layer of HTTP/2 is implemented as a reusable C library'
arch=('i686' 'x86_64')
url='https://nghttp2.org/'
license=('Expat')
depends=('glibc')
makedepends=('quilt')
conflicts=('nghttp2<1.20.0-2')
source=("https://github.com/nghttp2/nghttp2/releases/download/v${pkgver}/nghttp2-${pkgver}.tar.xz"
        "https://deb.debian.org/debian/pool/main/n/nghttp2/nghttp2_${_debver}-${_debrel}+deb11u1.debian.tar.xz")
sha512sums=('eac69ba356870a1cba420a06771082897be8dd40a68f4e04223f41f3d22626e4f5b3766d3dbcc496dd212be01f64c3ac280a2ebddd31dd88f7350c20f56e5d39'
            'e280509589588df21250da192c327f86b77af289b8b4c67c23545635f3a50032ff63f511dd4271883d12afca2aa5d4e4d9d6f358ed15340c8051c67064f5862d')

prepare() {
  cd nghttp2-$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/0001-Make-fetch-ocsp-response-use-python3.patch || true

    quilt push -av
  fi
  autoreconf -i
}

build() {
  cd nghttp2-$pkgver

  ./configure \
    --prefix=/usr \
    --disable-examples \
    --disable-python-bindings \
    --enable-lib-only
  make
}

package() {
  cd nghttp2-$pkgver/lib

  make DESTDIR="$pkgdir" install
  install -Dm644 ../COPYING -t "$pkgdir/usr/share/licenses/$pkgname"
}
