# Maintainer (Arch): Thomas Bächler <thomas@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=acl
pkgver=2.2.53
_debver=$pkgver
_debrel=10
pkgrel=2
pkgdesc="Access control list utilities, libraries and headers"
arch=('i686' 'x86_64')
url='https://savannah.nongnu.org/projects/acl'
license=('LGPL-2.1' 'GPL-2')
depends=('attr>=2.4.46')
makedepends=('quilt')
replaces=('xfsacl')
provides=('xfsacl')
conflicts=('xfsacl')
source=(https://download.savannah.nongnu.org/releases/$pkgname/$pkgname-$pkgver.tar.gz{,.sig}
        https://deb.debian.org/debian/pool/main/a/acl/acl_$_debver-$_debrel.debian.tar.xz)
sha512sums=('176b7957fe0e7618e0b7bf2ac5071f7fa29417df718cce977661a576fa184e4af9d303b591c9d556b6ba8923e799457343afa401f5a9f7ecd9022185a4e06716'
            'SKIP'
            'ebb01ff513a8b4bf78e09ebfdb53597d5aa8b4ff2510bdebba1dbea665e0ea91149938c2ac8e732120c78e8793b039d0d5bc6eb0b20c31363b4ce74f30de07d7')
validpgpkeys=('B902B5271325F892AC251AD441633B9FE837F581') # Mike Frysinger <vapier@gentoo.org>

prepare() {
  cd $pkgname-$pkgver

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply
    rm -v debian/patches/build-define-ENODATA.patch || true
    rm -v debian/patches/build-no-PATH_MAX.patch || true

    quilt push -av
  fi
  autoreconf -vfi
}

build() {
  cd $pkgname-$pkgver

  ./configure \
    --bindir=/bin \
    --libdir=/lib \
    --prefix=/usr
  make
}

package() {
  cd $pkgname-$pkgver

  make DESTDIR=$pkgdir install

  # install license files
  install -Dm644 doc/COPYING{,.LGPL} -t "$pkgdir"/usr/share/licenses/$pkgname
}
