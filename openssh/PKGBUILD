# Maintainer (Arch): Gaetan Bisson <bisson@archlinux.org>
# Contributor (Arch): Aaron Griffin <aaron@archlinux.org>
# Contributor (Arch): judd <jvinet@zeroflux.org>
# Contributor (Artix): artoo <artoo@cromnix.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>
# Contributor: Tobias Dausend <throgh@hyperbola.info>

pkgname=openssh
pkgver=8.4p1
_debver=$pkgver
_debrel=5
pkgrel=1
pkgdesc='Premier connectivity tool for remote login with the SSH protocol'
url='https://www.openssh.com/portable.html'
license=('Modified-BSD')
arch=('i686' 'x86_64')
makedepends=('linux-libre-lts-headers' 'quilt')
depends=('krb5' 'libedit' 'libxcrypt' 'ldns')
optdepends=('xenocara-xauth: X11 forwarding'
            'x11-ssh-askpass: input passphrase in X'
            'logger: message logging support')
backup=('etc/ssh/ssh_config'
        'etc/ssh/sshd_config'
        'etc/conf.d/sshd'
        'etc/sv/sshd/conf')
source=("https://ftp.openbsd.org/pub/OpenBSD/OpenSSH/portable/${pkgname}-${pkgver}.tar.gz"
        "https://deb.debian.org/debian/pool/main/o/openssh/openssh_${_debver}-${_debrel}+deb11u1.debian.tar.xz"
        'sshd.confd'
        'sshd.initd'
        'sshd.conf'
        'sshd.run'
        'libressl.patch')
sha512sums=('d65275b082c46c5efe7cf3264fa6794d6e99a36d4a54b50554fc56979d6c0837381587fd5399195e1db680d2a5ad1ef0b99a180eac2b4de5637906cb7a89e9ce'
            '3e89470ec920232277af734718927f4f7e3ead4697f38542780382f4d6c7a85718b3a885da63d27aaa8421793a39c8b6429b4a7c8574cd10c29db028d4a54d27'
            'b9ae816af54a55e134a9307e376f05367b815f1b3fd545c2a2c312d18aedcf907f413e8bad8db980cdd9aad4011a72a79e1e94594f69500939a9cb46287f2f81'
            'fe58e950514743a72467233ff2f2a63112c50e5db843d61e141a5ca3dd8ef8f42a616cd9de7748ae582054c47c2cc38ce48b638e2d88be39c1387f77e79c83e1'
            '00b4f6774fd6f864344bc087ff0fafdd485293527db2a4bef0ca2ba37639383f56977c794687c345b705462c2ad442062205938ec9e5e79647aeba2f9f530537'
            '28dba7575e0c8aeb73fc4c1cd154641a19455fb141b0758e8db3d327c75ae68784e16b7bede144150fccf4a51268c8ec5eebf6ca8ecac0abfdfa5e6228895188'
            '5ab2d1a805ea19d766d8b8707ee33f669a238cbfe4ee5426726d8ddcb3e14318df34c69f441d68f8baf2492c4aa7c31ec3adbd2c03c371534380f61471331dce')

prepare() {
	cd "${srcdir}/${pkgname}-${pkgver}"

	if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
		# Debian patches
		export QUILT_PATCHES=debian/patches
		export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
		export QUILT_DIFF_ARGS='--no-timestamps'

		mv "$srcdir"/debian .

		# Doesn't apply and seems unimportant
		rm -v debian/patches/debian-banner.patch || true
		rm -v debian/patches/debian-config.patch || true
		rm -v debian/patches/gnome-ssh-askpass2-icon.patch || true
		rm -v debian/patches/gssapi.patch || true
		rm -v debian/patches/no-openssl-version-status.patch || true
		rm -v debian/patches/package-versioning.patch || true
		rm -v debian/patches/selinux-role.patch || true
		rm -v debian/patches/ssh-agent-setgid.patch || true
		rm -v debian/patches/systemd-readiness.patch || true

		quilt push -av
	fi

	# Fix compatibility with LibreSSL
	# The patch is based on an equivalent patch for Void
	# https://github.com/void-linux/void-packages/commit/6118b964a594a8c8a97372ece63d9d41e329a7d9
	patch -p1 -i ../libressl.patch

	autoreconf -vfi
}

build() {
	cd "${srcdir}/${pkgname}-${pkgver}"

	./configure \
		--prefix=/usr \
		--libexecdir=/usr/libexec/ssh \
		--sysconfdir=/etc/ssh \
		--with-ldns \
		--with-libedit \
		--with-ssl-engine \
		--without-pam \
		--with-privsep-user=nobody \
		--with-kerberos5=/usr \
		--with-xauth=/usr/bin/xauth \
		--with-md5-passwords \
		--with-pid-dir=/run \
		--with-Werror

	make
}

check() {
	cd "${srcdir}/${pkgname}-${pkgver}"

	# Tests require openssh to be already installed system-wide,
	# also connectivity tests will fail under makechrootpkg since
        # it runs as nobody which has /bin/false as login shell.

	if [[ -e /usr/bin/scp && ! -e /.arch-chroot ]]; then
		make tests
	fi
}

package() {
	cd "${srcdir}/${pkgname}-${pkgver}"

	make DESTDIR="${pkgdir}" install

	ln -sf ssh.1.gz "${pkgdir}"/usr/share/man/man1/slogin.1.gz
	install -Dm644 LICENCE "${pkgdir}/usr/share/licenses/${pkgname}/LICENCE"

	install -Dm644 ../sshd.confd "${pkgdir}"/etc/conf.d/sshd
	install -Dm755 ../sshd.initd "${pkgdir}"/etc/init.d/sshd

	install -Dm644 ../sshd.conf "${pkgdir}"/etc/sv/sshd/conf
	install -Dm755 ../sshd.run "${pkgdir}"/etc/sv/sshd/run

	install -Dm755 contrib/findssl.sh "${pkgdir}"/usr/bin/findssl.sh
	install -Dm755 contrib/ssh-copy-id "${pkgdir}"/usr/bin/ssh-copy-id
	install -Dm644 contrib/ssh-copy-id.1 "${pkgdir}"/usr/share/man/man1/ssh-copy-id.1

	sed \
		-e '/^#ChallengeResponseAuthentication yes$/c ChallengeResponseAuthentication no' \
		-i "${pkgdir}"/etc/ssh/sshd_config
}
